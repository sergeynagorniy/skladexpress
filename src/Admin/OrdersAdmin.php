<?php

namespace App\Admin;

use App\Entity\OrderProducts;
use App\Entity\OrderStatus;
use App\Entity\OrderType;
use App\Entity\User;


use App\Form\OrderProductsFormType;
//use Doctrine\DBAL\Types\TextType;
use App\Helper\Arr;
use App\Service\invoicesService;
use App\Service\SkladUsaService;
use Doctrine\ORM\EntityManager;
use App\Entity\Order;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Admin\AbstractAdmin;

use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Sonata\AdminBundle\Form\Type\AdminType;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Form\InvoiceFormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Symfony\Contracts\Translation\TranslatorInterface;

class OrdersAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'orders';
    protected $baseRouteName = 'orders';
    protected $router;

    /** @var invoicesService */
    private  $invoicesService;

    protected $datagridValues = ['_page' => 1, '_sort_order' => 'DESC', '_sort_by' => 'createdAt'];
    const  CARRIER_CODES = [
        'Select company'                                => null,
        'DHL'                                           => 'dhl',
        'FedEx'                                         => 'fedex',
        'USPS'                                          => 'usps',
        'Parcel Priority with Delcon (14 - 21) days'    => 'apc',
        'UPS'                                           => 'ups',

        //            'Нова Пошта'                                    => 'nova-poshta',
        ];
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper->add('orderStatus');
        $datagridMapper->add('orderType',null ,['label' => 'OrderType']);
    }

    public function __construct(string $code, string $class, string $baseControllerName,UrlGeneratorInterface $router,invoicesService $invoicesService)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->router=$router;
        $this->invoicesService = $invoicesService;

    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
//        $query->andWhere(
//            $query->expr()->eq($query->getRootAliases()[0] . '.orderStatus', ':t')
//
//        );
        $query->andWhere($query->getRootAliases()[0].'.orderStatus'.' < :identifier');

        $query->setParameter('identifier', '4');
        return $query;
    }


    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper): void
    {

        $listMapper->addIdentifier('id')
            ->add('user', null, [], EntityType::class, [
                'class' => User::class,
                'choice_label' => 'name',
            ])
            ->add('order_status', null, [], EntityType::class, [
                'class' => OrderStatus::class,
                'choice_label' => 'order_status',
            ])
//            ->addIdentifier('sendFromAddress')
            ->addIdentifier('comment')
            ->addIdentifier('orderStatus')
            ->add('orderType', EntityType::class, [
                'class' => OrderType::class,
//                'placeholder' => 'Select type',
                'choice_label' => 'name',
                'choice_translation_domain' => 'messages',
                'label' => $this->trans( "OrderType"),
//                'required'=>true,
//                'attr'=>[
//                    'class'=>'form-control',
//                    'id'=>'order_type',
//                    'autocomplete'=>'off',
//                ],
            ])

            ->add('trNum',null,['label'=>'Трек для пользователя'])
//            ->add('trackingNumber',null,['label'=>'Трекномер Новой Почты'])
            ->add('systemNum',null,['label'=>'Трек системный(to coutry)'])
            ->add('systemNumInUsa',null,['label'=>'Трек системный(to city)'])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper): void
    {
        $invoicesStr='';
        /* @var Order $object*/
        $object=$this->getSubject();

        if (!empty($object->getInvoices())){
            $invoicesStr='<table class="table"><thead>'.
            '<th>'.$this->trans( "Price").'</th>'.
            '<th>'.$this->trans( "Comment").'</th>'.
            '<th>'.$this->trans( "Status").'</th>'.
                '</thead><tbody>';
            foreach ($object->getInvoices() as $invoice){
                /* @var \App\Entity\Invoices $invoice */
                $invoiceStatus=($invoice->isPaid())?
                    '<span class="label label-success">'.$this->trans( "paid").'</span>'
                    :
                    '<span class="label label-danger">'.$this->trans( "nopaid").'</span>';
                $invoicesStr .='<tr>'.
                        '<td>'.$invoice->getPrice().'</td>'.
                        '<td>'.$invoice->getComment().'</td>'.
                        '<td>'.$invoiceStatus.'</td>'.
                        '</tr>';
            }
            $invoicesStr .='</tbody></table>'.
            '<a class="btn btn-info" href="'.$this->router->generate("invoices_add-invoice",["order"=>$object->getId()],UrlGeneratorInterface::ABSOLUTE_URL).'">'.$this->trans("Add Invoice").'</a>';
        }
        $productStr ='';
        if (!empty($object->getProducts())){
            $productStr='<table class="table table-striped"><thead>'.
                '<th>'.$this->trans( "Link").'</th>'.
                '<th>'.$this->trans( "Price").'</th>'.
                '<th>'.$this->trans( "Count").'</th>'.
                '<th>'.$this->trans( "trNum").'</th>'.
                '</thead><tbody>';
            foreach ($object->getProducts() as $product){
                /* @var \App\Entity\OrderProducts $product */
                $productStr .='<tr>'.
                    '<td> <a target="_blank" class = "url-for-ajax" href = "'.$product->getDescEn().'">'.$product->getDescEn().'</a> 
                       <div class="url-ajax-load-info" data-id=" ">
                        <div class="row">
                            <div class="col-md-4 image-og" > </div>
                            <div class="col-md-8"><p class="title-og"></p><p class="description-og"></p></div>
                        </div>
                    </div></td>'.
                    '<td><input name="product['.$product->getId().'][price]" value ="'.$product->getPrice().'"/>'.'</td>'.
                    '<td><input name="product['.$product->getId().'][count]" value ="'.$product->getCount().'"/>'.'</td>'.
                    '<td><input name="product['.$product->getId().'][trNum]" value ="'.$product->getTrNum().'"/></td>'.
                    '</tr>';
            }
            $productStr .='</tbody></table>';
        }
        $userFieldOptions = [];
        $orderStatusFieldOptions = [];
        $addressesFieldOptions = [];
        $shippingCostUserSelect = "";
        if ($object->getCalculatedData()){
            $dataArray = $object->getCalculatedDataArray();
            $shippingCostUserSelect = "<p>Результат расчет</p>"
                ."<p>Econom</p>"
                ."<p>".$object->getEconomPrice()."</p>"
                ."<p>".Arr::get($dataArray,"econom.description",'')."</p>"
                ."<p>Express</p>"
                ."<p>".$object->getExpressPrice()."</p>"
                ."<p>".Arr::get($dataArray,"express.description",'')."</p>";
        }
        $formMapper
            ->add('user', ModelType::class, $userFieldOptions)
            ->add('orderType', EntityType::class, [
                'class' => OrderType::class,
                'placeholder' => 'Select type',
                'choice_label' => 'name',
                //'choice_translation_domain' => 'messages',
                'label' => $this->trans( "OrderType"),
                'required'=>true,
                'attr'=>[
                    'class'=>'form-control',
                    'id'=>'order_type',
                    'autocomplete'=>'off',
                ],
            ])
            ->add('orderStatus', ModelType::class, $orderStatusFieldOptions)
//            ->add('orderShippingCosts', ModelType::class, [])
//            ->add('addresses', ModelType::class, $addressesFieldOptions)
            ->add('zip')
            ->add('countryCode')
            ->add('city')
            ->add('regionOblast')
            ->add('address')
            ->add('receiverName')
            ->add('phone')
            ->add('email')
            ->add('productsStr', TextType::class,[
                'mapped'=>false,
                'label'=>'Products',
                'required'=>false,
                'attr'=>['class'=>'hide'],
                //'label_attr'=>['class'=>'hideddd'],
            ],['help'=>$productStr])
            ->add('trackingNumber',null,['label'=>'Трек новой почты'])
            ->add('trNum',null,['label'=>'Трек для пользователя','disabled'=>true,'required'=>false])
            /*
            ->add('companySendToUsa', ChoiceType::class, [
                        'choices'  => $this::CARRIER_CODES,
                        'label'=>'Компания доставки(Посылка едет в страну назначения)'
                 ]
            )
            */
            ->add('systemNum',null,['label'=>'Трек системный(Тот что меняет админ)','required'=>false])
            /*
            ->add('companySendInUsa', ChoiceType::class, [
                    'choices'  => $this::CARRIER_CODES,
                    'label'=>'Компания доставки(Посылка едет к аддресу назначения)'
                ]
            )
            */
            ->add('systemNumInUsa',null,['label'=>'Трек системный(Посылка едет к аддресу назначения)','required'=>false])
            ->add('volumeWeigth')
            ->add('declareValue')
//            ->add('sendFromAddress')
//            ->add('sendFromIndex')
//            ->add('sendFromCity')
//            ->add('sendFromPhone')
//            ->add('sendFromEmail')
//            ->add('sendDetailPlaces')
            ->add('sendDetailWeight')
            ->add('sendDetailLength')
            ->add('sendDetailWidth')
            ->add('sendDetailHeight')
            ->add('comment')
            ->add('email')
//            ->add('shipDate',)
            ->add('shipDate', DateType::class, [
                'widget' => 'choice',
            ])
//            ->add('address')
            ->add('shippingCosts',null,[],["help"=>$shippingCostUserSelect])
            ->add('deliveryStatus')
//            ->add('country')
//            ->add('fromCountry')
//            ->add('city')
//            ->add('zip')
//            ->add('towarehouse')
//            ->add('quantity')
            
            ->add('invoicesStr', TextType::class,[
                'label'=>'Invoices',
                'required'=>false,
                'attr'=>['class'=>'hide'],
                //'label_attr'=>['class'=>'hideddd'],
                ],['help'=>$invoicesStr])
            ;
//            if (!empty($object->getOrderStatus()) && ($object->getOrderStatus()->getStatus() == 'paid')) {
//                $sendBlock = '<a class="btn btn-warning" href="'.$this->router->generate("post_sendtosklad",["id"=>$object->getId()],UrlGeneratorInterface::ABSOLUTE_URL).'">Send Order To Sklad</a>';
//                $formMapper
//                    ->add('sendBlock', TextType::class,[
//                        'label'=>'Send To Sklad',
//                        'required'=>false,
//                        'mapped' => false,
//                        'attr'=>['class'=>'hide'],
//                    ],['help'=>$sendBlock])
//                ;
//            }
    }

    /**
     * @param $order
     */
    public function preUpdate($order) {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getModelManager()->getEntityManager($this->getClass());
        /** @var  Order $order */
        $request = $this->getRequest();
        $productsArr= $request->get('product',[]);

        if ($order->getProducts()){
            /** @var OrderProducts $product */
            foreach ($order->getProducts() as $product){
                $productFromForm = Arr::get($productsArr,$product->getId(),[]);
                $product
                    ->setTrNum(Arr::get($productFromForm,'trNum',null))
                    ->setCount(Arr::get($productFromForm,'count',null))
                    ->setPrice(Arr::get($productFromForm,'price',null))
                ;
                $entityManager->persist($product);

            }
        }

        $currentStatus = $order->getOrderStatus();
        if(!empty($order->getOrderStatus())&&($order->getOrderStatus()->getStatus() == 'formed')){

            if (empty($order->getTrNum())){
                $order->setTrNum("EP".($order->getId()+57354658)."UA");
            }
            $service = new SkladUsaService();
            $result = $service->sendOrderToSklad($order);

            if(json_decode($result)->status == 'success') {
                $this->getRequest()->getSession()->getFlashBag()->add("success", "Заказ отправлен на склад");
              /*  $orderStatus = $entityManager->getRepository(OrderStatus::class)->findOneBy(['status' => 'complit']);
                $order->setOrderStatus($orderStatus);
                $entityManager->persist($order);
                $entityManager->flush();
              */
            } else {
                $this->getRequest()->getSession()->getFlashBag()->add("error", "Заказ не отправлен на склад. Возникла ошибка");
                /*
                $orderStatus = $entityManager->getRepository(OrderStatus::class)->findOneBy(['status' => 'paid']);
                $order->setOrderStatus($currentStatus);
                //$order->setOrderStatus($orderStatus);
                $entityManager->persist($order);
                $entityManager->flush();
                */
            }

        }

        $entityManager->flush();
        $this->invoicesService->createInvoice($order);
        $this->invoicesService->createInvoice($order,true);
    }
}
