<?php
namespace App\Command;



use App\Entity\BalanceLog;
use App\Service\BalanceService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

use App\Command\Traits\clearTrait;
use App\Command\Traits\ControlProcessTrait;

use Symfony\Component\Console\Command\Command;

class BalanceTailsCommand extends Command
{

    use clearTrait,ControlProcessTrait;

    /** @var EntityManagerInterface  */
    private $entityManager;
    /** @var BalanceService  */
    private $balanceService;

    public function __construct(EntityManagerInterface $entityManager,BalanceService $balanceService)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->balanceService = $balanceService;


    }

    protected function configure()
    {
        $this
            ->setDescription('Balance crop operation. The command to run on server crontable.')
            ->setName('balance:autopayfomlog')
            ->addOption('count', null, InputOption::VALUE_OPTIONAL, 'Step count', 3)
            ->addOption('countByStep', null, InputOption::VALUE_OPTIONAL, 'Count Element By step',30)
            ->addOption('period', null, InputOption::VALUE_OPTIONAL, 'Period sleep in seconds', 10);;
    }



    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->lock("balance-tail");
        $countByStep = $input->getOption('countByStep',30);
        $period = $input->getOption('period',10);
        $count = $input->getOption('count',3);
        for ($i = 1; $i <= $count; $i++) {
            $this->doProcessFromLog($countByStep);
            sleep($period);
        }
    }

    private function doProcessFromLog($countByStep)
    {


        $tailLogs = $this->entityManager->getRepository(BalanceLog::class)
            ->createQueryBuilder('bl')
            ->where('bl.process = :process')
            ->setParameter("process",false)
            ->setMaxResults($countByStep)
            ->orderBy('bl.createdAt',"ASC")
            ->groupBy('bl.user')
            ->getQuery()
            ->getResult();
        if (count($tailLogs)>0){
            /** @var BalanceLog $tailLog */
            foreach ($tailLogs as $tailLog){
                $tailLog->setProcess(true);
                $this->entityManager->persist($tailLog);
            }
            $this->entityManager->flush();
        }


        if ($tailLogs){

            /** @var BalanceLog $tailLog */
            foreach ($tailLogs as $tailLog){
                echo $tailLog->getId()."\r\n";
                 $this->balanceService->moneyFromLog($tailLog);

            }

        }
        $this->clear($this->entityManager);
        return true;
    }

}