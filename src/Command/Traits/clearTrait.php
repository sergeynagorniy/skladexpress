<?php

namespace App\Command\Traits;



use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

trait clearTrait
{

    /**
     * @param EntityManager $em
     * @return boolean
     */
    public function clear($entityManager)
    {

        $entityManager->clear();
        $entityManager->getConnection()->close();

        gc_collect_cycles();

        return true;
    }


}
?>