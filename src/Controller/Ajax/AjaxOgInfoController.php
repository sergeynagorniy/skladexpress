<?php

namespace App\Controller\Ajax;


use App\Entity\Invoices;
use App\Entity\Order;
use App\Entity\OrderProducts;
use App\Entity\Transaction;
use App\Helper\Arr;
use App\Helper\OpenGraph;
use App\Service\AuthorizeDotNetService;
use App\Service\invoicesService;
use App\Service\PaypalIpnService;
use App\Service\PaypalRESTService;
use App\Service\TransactionService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;



class AjaxOgInfoController extends AbstractController
{
      /** @var EntityManagerInterface */
    private $em;
     public function __construct(
         EntityManagerInterface $em
     )
     {
        $this->em = $em;
     }

    /**
     * @Route("ajax/get-url-info", name="ajax_get_url_info")
     * @param Request $request
     * @return JsonResponse
     */
    public function ajaxGetUrlInfo(Request $request)
    {

        $link   = $request->get("link",null);
        $graph = [];
        if ($link) {
            /** @var OrderProducts $orderProducts */
            $orderProducts = $this->em->getRepository(OrderProducts::class)->findOneBy(["descEn" => $link]);

            if ($orderProducts){
                $ogInfo = $orderProducts->getOgInfo();
                if ($ogInfo && is_array($ogInfo) &&  count($ogInfo)>0){
                    $graph = $ogInfo;
                }else{
                    $graph = $this->getOgInfoByUrl($link);
                    $orderProducts->setOgInfo($graph);
                    $this->em->persist($orderProducts);
                    $this->em->flush();
                }
            }else{
                $graph = $this->getOgInfoByUrl($link);
            }
        }
//        $graph = OpenGraph::utf8ize($graph);
           return new JsonResponse($graph);
    }

    public function getOgInfoByUrl($link)
    {
        $graph = OpenGraph::fetch($link);

          return [
            "image"=>($graph && $graph->__isset("image"))?$graph->image:null,
            "title"=>($graph && $graph->__isset("title"))?$graph->title:null,
            "description"=>($graph && $graph->__isset("description"))?$graph->description:null,
            "url"=>($graph && $graph->__isset("url"))?$graph->url:$link
            ];
    }
}
