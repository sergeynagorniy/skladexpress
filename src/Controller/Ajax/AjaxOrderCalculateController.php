<?php

namespace App\Controller\Ajax;


use App\Entity\Address;
use App\Entity\Invoices;
use App\Entity\Order;
use App\Entity\OrderProducts;
use App\Entity\Transaction;
use App\Helper\Arr;
use App\Helper\OpenGraph;
use App\Service\AuthorizeDotNetService;
use App\Service\invoicesService;
use App\Service\PaypalIpnService;
use App\Service\PaypalRESTService;
use App\Service\SkladUsaService;
use App\Service\TransactionService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;



class AjaxOrderCalculateController extends AbstractController
{
      /** @var EntityManagerInterface */
    private $em;
     public function __construct(
         EntityManagerInterface $em
     )
     {
        $this->em = $em;
     }

    /**
     * @Route("ajax/order-calculate", name="ajax_post_calculate")
     * @param Request $request
     * @return JsonResponse
     */
    public function orderCalculate(Request $request)
    {


        $orderForm   = $request->get("order_form",[]);
        $addresses = Arr::get($orderForm,'addresses',null);
        /** @var Address $addressEntity */
        $addressEntity = $this->em->getRepository(Address::class)->find($addresses);


        $service = new SkladUsaService();
        $weight = Arr::get($orderForm,'sendDetailWeight',0);
        $weight = $weight/1000;
        $data = [
            "senderCountryCode" => "UA",
            "length" => Arr::get($orderForm,'sendDetailLength',null),
            "height" => Arr::get($orderForm,'sendDetailHeight',null),
            "width" => Arr::get($orderForm,'sendDetailWidth',null),
            "weight" => $weight,
            "countryCode" => $addressEntity->getCountry()->getShortName(),
            "stateCode" => $addressEntity->getRegionOblast(),
            "zipCode" => $addressEntity->getZip(),
            "city" => $addressEntity->getCity(),
            "clientPayPal" => "0"
        ];

        $result = $service->calculate($data);
        $consolidationPrice = Arr::get($result,'data.shippingPrices.consolidation',[]);

        $maxPrice = 0;
        $resultPrice = [];
        foreach ($consolidationPrice as $cKey => $cPrice){
            $price = Arr::get($cPrice,"costs.price",0);
            $fee = Arr::get($cPrice,"costs.fee",0);
            $priceSum = $price + $fee;
            if ($priceSum > $maxPrice){
                $maxPrice = $priceSum;
                $resultPrice = $cPrice;
                $resultPrice["key"] = $cKey;
                $resultPrice["shippingCost"] = $maxPrice;
            }
        }

        $expressPrice = Arr::get($result,'data.shippingPrices.express',[]);
        $express = [];
        foreach ($expressPrice as $ePrice){
            $eKey = Arr::get($ePrice,"key",null);
            if ($eKey == 'dhlExpToBuyer'){
                $price = Arr::get($ePrice,"costs.price",0);
                $fee = Arr::get($ePrice,"costs.fee",0);
                $priceSum = $price + $fee;
                $express = $ePrice;
                $express["shippingCost"] = $priceSum;
                continue;
            }
        }


        return new JsonResponse(["econom"=>$resultPrice,"express"=>$express]);
    }

}
