<?php

namespace App\Controller\Ajax;


use App\Entity\Invoices;
use App\Entity\Order;
use App\Entity\OrderProducts;
use App\Entity\Transaction;
use App\Helper\Arr;
use App\Service\AuthorizeDotNetService;
use App\Service\invoicesService;
use App\Service\PaypalIpnService;
use App\Service\PaypalRESTService;
use App\Service\TransactionService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;



class AjaxPayFromBalanceController extends AbstractController
{
    /** @var invoicesService */
    private  $service;
    /** @var  AuthorizeDotNetService */
    private  $authorizeDotNetService;

    /** @var EntityManagerInterface */
    private $em;
     public function __construct(
         invoicesService $service,
         TransactionService $transactionService,
         EntityManagerInterface $em
     )
     {
        $this->service = $service;
        $this->transactionService = $transactionService;
        $this->em = $em;
     }

    /**
     * @Route("post/ajax/pay-from-balance", name="ajax_pay_from_balance")
     * @param Request $request
     * @return JsonResponse
     */
    public function createPaypalOrder(Request $request)
    {

        $id   = $request->get("invoiceid",null);

        /** @var Invoices $invoice */
        $invoice = $this->em->getRepository(Invoices::class)->find($id);

        if ($invoice){
            if ($invoice->getPrice()>0 && $invoice->getPrice() <= $this->service->getUserFromInvoice($invoice)->getBalance()) {
                try {
                    $this->service->payInvoiceFromBalance($invoice);
                    return new JsonResponse(["sucess"]);
                }catch(\Exception $exception){
                    return new JsonResponse(["mess"=>$exception->getMessage()], 500);
                }

            }else{
                return new JsonResponse(["mess"=>"no money"], 500);
            }
        }

        return new JsonResponse(["mess"=>"other error"], 500);
    }

}
