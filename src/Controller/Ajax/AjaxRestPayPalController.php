<?php

namespace App\Controller\Ajax;


use App\Entity\Invoices;
use App\Entity\Order;
use App\Entity\OrderProducts;
use App\Entity\Transaction;
use App\Helper\Arr;
use App\Service\AuthorizeDotNetService;
use App\Service\PaypalIpnService;
use App\Service\PaypalRESTService;
use App\Service\TransactionService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

use App\Traits\PriceTrait;
define("LOG_PAYPAL", getcwd() . "/../payPay.log");

class AjaxRestPayPalController extends AbstractController
{
    use PriceTrait;

    /** @var PaypalRESTService */
    private  $servicePaypal;
    /** @var PaypalIpnService */
    private  $service;
    /** @var TransactionService */
    private  $transactionService;
    /** @var  AuthorizeDotNetService */
    private  $authorizeDotNetService;

    /** @var EntityManagerInterface */
    private $em;
     public function __construct(
         PaypalIpnService $service,
         PaypalRESTService $servicePaypal,
         TransactionService $transactionService,
         AuthorizeDotNetService $authorizeDotNetService,
         EntityManagerInterface $em
     )
     {
        $this->service = $service;
        $this->transactionService = $transactionService;
        $this->authorizeDotNetService = $authorizeDotNetService;
        $this->em = $em;
        $this->servicePaypal = $servicePaypal;
     }

    /**
     * @Route("post/ajax/create-order", name="ajax_paypal_create_order")
     * @param Request $request
     * @return JsonResponse
     */
    public function createPaypalOrder(Request $request)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $type  = $request->get("elType",null);
        $id    = $request->get("elId",null);
        $params = $request->request->all();
        $result = $this->createOrder($type,$id,$params);

        return new JsonResponse($result);
    }


    /**
     * @Route("post/ajax/save-order", name="ajax_paypal_save_order")
     * @param Request $request
     * @return JsonResponse
     */
    public function savePaypalOrder(Request $request)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $orderId  = $request->get("orderId",null);
        $jsonArr  = $request->get("json_arr",null);
        error_log($jsonArr . PHP_EOL, 3, LOG_PAYPAL);
        $arr = ($jsonArr) ? json_decode($jsonArr,true) : [];

       $orderIdCaptures = Arr::get( $arr,"purchase_units.0.payments.captures.0.id",null);
       if ($orderIdCaptures) $orderId=$orderIdCaptures;
            try {
                $transaction_data = $this->service->parseTransaction($orderId);


                $this->transactionService->setData($transaction_data);
                $invoiceId =  $this->transactionService->getClearInvoiceId();

                /** @var  Invoices $invoice */
                $invoice = $em->getRepository(Invoices::class)->find($invoiceId);

                if ($invoice){
                    $this->transactionService->setInvoice($invoice);
                    /** @var Order $order */
                    $order = ($invoice && $invoice->getOrderId())?$invoice->getOrderId():false;
                    if ($order) $this->transactionService->setUser($order->getUser());
                    else $this->transactionService->setUser($invoice->getUser());
                }
                /* @var Transaction $trAutorize */
                $trAutorize = $this->transactionService->saveTransaction();

            } catch (\Exception $e) {
                echo $e->getMessage();
            }



        return new JsonResponse('');
    }

    /**
     * @param PaypalRESTService $service
     * @param $type
     * @param $id
     * @param array $confArr
     * @return array
     */
    public function createOrder($type,$id,$confArr=[])
    {
        try {
            if ($type && $id ) {
                $purchaseunit = [];
                $applicationcontext = null;

                switch ($type) {
                    case 'paypalAddToBalance':
                        $purchaseunit = $this->getPurchaseUnitForpaypalAddToBalance($id,$confArr);
                        break;
                    case 'payInvoice':
                        $purchaseunit = $this->getPurchasePayInvoice($id,$confArr);
                        break;
                    default:
                        break;
                }

                $createResponse = $this->servicePaypal->createOrder($purchaseunit, $applicationcontext);
                return $createResponse;
            }
            else throw new \InvalidArgumentException("Not set type of order or id");
        }catch (\Exception $exception){
            return ["error"=>$exception->getMessage()];
        }
    }

    public function getPurchaseUnitForpaypalAddToBalance($id,$confArr=[])
    {
        /** @var \Doctrine\ORM\EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        $amount = $confArr['amount'] ?? null;
        $userId = $confArr['userId'] ?? null;
        /** @var \App\Entity\User $user */
        $user = $entityManager
            ->getRepository(\App\Entity\User::class)
            ->find($userId);
        $invoice = $this->authorizeDotNetService->createInvoiceForAddToBalance($amount,$user,"Warehouse Fulfillment Service Fees",true);
        if ($invoice && !empty($userId) && $amount){
            $purchaseunit = new \App\Service\PayPalRestApi\PurshaseUnit();
            $money =  new \App\Service\PayPalRestApi\Money($amount);
            $amount =  new \App\Service\PayPalRestApi\Money($amount);
            //$amount->addBreakdownTotalSumm($amount->getData());

            $item =  new \App\Service\PayPalRestApi\Item();
            $item
                ->setName("Warehouse Fulfillment Service Fees")
                ->setDescription("Warehouse Fulfillment Service Fees")
                ->setQuantity(1)
                ->setUnitAmount($money->getData());
            $purchaseunit
                ->setDescription("Warehouse Fulfillment Service Fees")
                ->setAmount($amount->getData())
                // ->addItem($item->getData())
                ->setCustomId('EXPRESSINVOICE_'.$invoice->getId()."_".time())
                //->setInvoiceId("SKLAD_UID".$user->getId()."_".md5('paypalAddToBalance:'.$user->getId().":".time()))
                ->setInvoiceId(date('dmYHis')."000".$invoice->getId() )
            ;
            return $purchaseunit->getData();
        }else{
            throw new \InvalidArgumentException('Not found user');
        }
    }



    public function getPurchasePayInvoice($id,$confArr=[])
    {
//        $amount = $confArr['amount'] ?? null;
//        $itemsTmp = $confArr['items'] ?? '';
        $elId = $confArr['elId'] ?? '';

//        $items = json_decode($itemsTmp,true);

        /** @var  Invoices $invoice */
        $invoice = $this->em->getRepository(Invoices::class)->find($elId);

        if ($invoice){
            $purchaseunit = new \App\Service\PayPalRestApi\PurshaseUnit();


            if ($invoice->isShippingPay() && $invoice->getPrice()>0){

                $price = $this->getPriceWithCommissionPayPal($this->getPriceWithCommissionPayPal($invoice->getPrice()));
                $amount =  new \App\Service\PayPalRestApi\Money($price);

//                $amount->addBreakdownTotalSumm($amount->getData());
                $purchaseunit
                    ->setDescription("Warehouse Fulfillment Service Fees #".$elId)
                    ->setAmount($amount->getData())
                    ->setCustomId('EXPRESSINVOICE_'.$invoice->getId()."_".time())
                    ->setInvoiceId(date('dmYHis')."000".$invoice->getId() )
                ;
            }elseif(!$invoice->isShippingPay() && $invoice->getPrice()>0 && $invoice->getOrderId()){
                /** @var Order $order */
                    $order = $invoice->getOrderId();
                $descriptionArr = [];
                $priceAll=0;

                $orderTotal = $order->getOrderTotalSum();
                if ($orderTotal = $invoice->getPrice()) {
                    /** @var OrderProducts $item */
                    foreach ($order->getProducts() as $item) {
                        $itemElement = new \App\Service\PayPalRestApi\Item();
                        $price = $this->getPriceWithCommissionPayPal($this->getPriceWithCommissionPayPal($item->getPrice()));
                        $priceAll += ($price*$item->getCount());
                        $money = new \App\Service\PayPalRestApi\Money($price ?? 0);
                        $itemElement
                            ->setName($item->getDescEn() ?? '')
                            ->setDescription($item->getDescEn() ?? '')
                            ->setQuantity($item->getCount() ?? 0)
                            ->setUnitAmount($money->getData());
                        $descriptionArr[] = $item->getDescEn() ?? '';
                        $purchaseunit->addItem($itemElement->getData());
                    }
                }else{
                    $priceAll = $invoice->getPrice();
                    $descriptionArr = ["Add Pay in expresspochta.com order #".$order->getId()];
                }
                $amount =  new \App\Service\PayPalRestApi\Money($priceAll);

                $amount->addBreakdownTotalSumm($amount->getData());
                $purchaseunit
                    ->setDescription(implode("",$descriptionArr))
                    ->setAmount($amount->getData())
                    // ->addItem($item->getData())
                    ->setCustomId('EXPRESSINVOICE_'.$invoice->getId().":".time())
                    ->setInvoiceId(date('dmYHis')."000".$invoice->getId() )
                ;
            }

            return $purchaseunit->getData();

        }else{
            throw new \InvalidArgumentException('Not found data for set payments');
        }
    }

}
