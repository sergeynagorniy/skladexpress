<?php

namespace App\Controller;

use App\Controller\CabinetController;
use App\Entity\Country;
use App\Entity\Coupon;
use App\Entity\ExpressDeliveryPrice;
use App\Entity\Order;
use App\Entity\User;
use App\Entity\Address;
use App\Entity\Invoices;
use App\Entity\OrderStatus;
use App\Entity\OrderType;
use App\Entity\PriceForDeliveryType;
use App\Entity\DeliveryPrice;
use App\Entity\OrderProducts;
use App\Form\SupportType;
use App\Helper\Arr;
use App\Service\ConvertCurrencyPrivatBankService;
use App\Service\DhlDeliveryService;
use App\Service\invoicesService;
use App\Service\SkladUsaService;
use Doctrine\ORM\EntityManager;
use Swift_Mailer;
use Swift_SmtpTransport;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Form\OrderFormType;

use Knp\Component\Pager\PaginatorInterface;
use Doctrine\Common\Collections\ArrayCollection;

use App\Service\LiqPayService;
use App\Service\AuthorizeDotNetService;

//use Swift_Message;
/**
 * @Route("/post/parcels")
 */
class ParcelsController extends CabinetController
{
    private $useCoupon=false;
    private $couponData=[];
    private $isVip=false;

    /**
     * new orders list
     * @Route("/", name="post_parcels")
     */
    public function parcelsAction(Request $request, PaginatorInterface $paginator ,AuthorizeDotNetService $authorizeservice): Response
    {
        $this->getTemplateData();
        $this->optionToTemplate['page_id']='post_parcels';
        $this->optionToTemplate['page_title']='New Parcels List';

        $entityManager = $this->getDoctrine()->getManager();

        $orders = $entityManager
            ->getRepository(Order::class)
            ->getNewOrders($this->user->getId());
        $ordersList = $paginator->paginate(
            $orders,
            $request->query->getInt('page', 1),
            20
        );
        $totalItemCount = $ordersList->getTotalItemCount();

        return $this->render('cabinet/parcels/parcels.html.twig'
            , array_merge($this->optionToTemplate,[
                'items'=>$ordersList,
                'totalItemCount'=>$totalItemCount,
            ])
        );
    }

    /**
     * sent orders list
     * @Route("/send", name="post_parcels_send")
     */
    public function parcelsSendAction(Request $request, PaginatorInterface $paginator): Response
    {
        $this->getTemplateData();
        $this->optionToTemplate['page_id']='post_parcels_send';
        $this->optionToTemplate['page_title']='Send Parcels List';

        $entityManager = $this->getDoctrine()->getManager();

        $orders = $entityManager
            ->getRepository(Order::class)
            ->getSendOrders($this->user->getId());

        $ordersList = $paginator->paginate(
            $orders,
            $request->query->getInt('page', 1),
            20
        );

        $totalItemCount = $ordersList->getTotalItemCount();

        return $this->render('cabinet/parcels/parcels.html.twig'
            , array_merge($this->optionToTemplate,[
                'isSend'=>1,
                'items'=>$ordersList,
                'totalItemCount'=>$totalItemCount,
            ])
        );
    }

    /**
     * @Route("/create", name="post_parcels_create")
     */
    public function parcelsCreateAction(Request $request, invoicesService $invoicesService,TranslatorInterface $translateService): Response
    {
        $this->getTemplateData();
        $errors =[];
        $this->optionToTemplate['page_id']='post_parcels_create';
        $this->optionToTemplate['page_title']='Parcels Create';
        $entityManager = $this->getDoctrine()->getManager();

        $order = new Order();


        $orderForm=$request->request->get('order_form',false);
        if ($orderForm)
        {
            if ($products=$orderForm['products']??false){
                foreach($products as $product){
                    $orderProduct=new OrderProducts();
                    $order->addProduct($orderProduct);
                }
            }
        }
        else{
            $orderProduct=new OrderProducts();
            $order->addProduct($orderProduct);
        }

        $maxWeightEconom = $this->getDoctrine()
            ->getRepository(PriceForDeliveryType::class)
            ->findMaxWeight();
        $maxWeightEconomVip = $this->getDoctrine()
            ->getRepository(PriceForDeliveryType::class)
            ->findMaxWeight();


        $form = $this->createForm(OrderFormType::class, $order, ['attr'=>['user' => $this->user,'maxWeightEconom' => $maxWeightEconom[1], 'maxWeightEconomVip' => $maxWeightEconomVip[1]]]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $data = $form->getData();
            $this->useCoupon=$this->getDiscountCoupon($data,true);
            $this->isVip=($this->useCoupon || $this->user->isVip())?true:false;

            $declareValue=0;
            if ($order->getProducts()) {
                foreach ($order->getProducts() as $product) {
                    if (empty($product->getDescEn())) {
                        $order->removeProduct($product);
                        $entityManager->remove($product);
                        $entityManager->persist($order);
                    } else {
                        $product->setOrderId($order);
                        $entityManager->persist($product);
                        $declareValue=$declareValue+$product->getCount()*$product->getPrice();
                    }
                }
            }
            unset($product);
            $order->setDeclareValue($declareValue);
            list($shipCost,$volume)=$this->CalculateShipCost($order);
//            $order->setShippingCosts($shipCost);
            $order->setVolumeWeigth($volume);

            $order->setUser($this->user);
            $orderStatus=$entityManager->getRepository(OrderStatus::class)->findOneBy(['status'=>'new']);
            $order->setOrderStatus($orderStatus);
            $order->setOrderType($this->getOrderTypeFromRequest($request));
            $orderCost = $this->getOrderPrice($order,$orderForm);
            $order->setShippingCosts($orderCost);

            $entityManager->persist($order);
            $entityManager->flush();

            $this->addFlash(
                'success',
                $translateService->trans("Orders Added sucusfull")
            );

            $invoicesService->createInvoice($order,false);

            if ($order->getShippingCosts()>0)  $invoicesService->createInvoice($order,true);

                return $this->redirectToRoute('post_parcels');
        }elseif ($form->isSubmitted() && !$form->isValid()){
            $errors = $form->getErrors(true);
        }
        $twigoption=array_merge($this->optionToTemplate,['form' => $form->createView(),
            'useCoupon'=>$order->isUseCoupon(),
            "calculate"=>$order->isCalculate(),
            "economPrice"=>$order->getEconomPrice(),
            "expressPrice"=>$order->getExpressPrice(),
            "deliveryType"=>($order->getOrderType())?$order->getOrderType()->getCode():null,
            'error' => $errors,]);
        return $this->render('cabinet/parcels/editform.html.twig', $twigoption);

    }

    /**
     * @param Request $request
     * @return OrderType|mixed|null|object
     */
    public function getOrderTypeFromRequest(Request $request)
    {
        $orderTypeR = $request->get("deliveryType","express");

        /** @var OrderType $orderType */
        $orderType = $this->getDoctrine()->getManager()->getRepository(OrderType::class)->findOneBy(["code"=>'express']);
        switch($orderTypeR){
            case "express":

                break;
            case "econom":
                $orderType = $this->getDoctrine()->getManager()->getRepository(OrderType::class)->findOneBy(["code"=>'econom']);
                break;
        }
        return $orderType;

    }

    private function getDiscountCoupon($order,$save=false)
    {
        /** @var Order $order */
        $result = false;
        $couponCode=$order->getCoupon();
        if (!empty($couponCode)) {
            $entityManager = $this->getDoctrine()->getManager();
            $originalData = $entityManager->getUnitOfWork()->getOriginalEntityData($order);
            $originalCupon = $originalData['Coupon'] ?? false;
            /** @var Coupon $couponObject */
            $couponObject = $entityManager->getRepository(Coupon::class)->findOneBy(['Code' => $couponCode]);

            if (!empty($couponObject)) {
                if (
                    $originalCupon
                    &&
                    $originalCupon == $couponObject->getCode()
                ) {
                    // use this cupon
                    $result = true;
                    $this->couponData = [
                        'quantity'=> $couponObject->getQuantity()
                    ];
                } else {
                    // use new cupon
                    if (
                        empty($couponObject->getUserCoupon())
                        ||
                        (
                            $couponObject->getUserCoupon() == $this->user
                            &&
                            $couponObject->getQuantity() > 0
                        )
                    ) {
                        $couponObject->setUserCoupon($this->user);

                        if ($save) {
                            $couponObject->setQuantity($couponObject->getQuantity() - 1);
                            $entityManager->persist($couponObject);
                        }
                        $result = true;
                        $this->couponData = [
                            'quantity'=> $couponObject->getQuantity()
                        ];

                    }
                    else{
                        $this->couponData = [
                            'quantity'=> 0
                        ];
                    }
                }
            }else{
                $this->couponData = [
                    'error'=> 'error'
                ];
            }
        }
        else $this->couponData = [
            'empty'=> 'true'
        ];
        return $result;
    }


    /**
     * @param Order $order
     * @param $orderForm
     * @return bool|float|int|string|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function getOrderPrice($order,$orderForm){


              if($order->getOrderType() && $order->getOrderType()->getCode() == 'express') {
                  return $order->getExpressPrice();
              }
              elseif($order->getOrderType() && $order->getOrderType()->getCode() == 'econom') {
                return $order->getEconomPrice();
               }
        return 0;
        $entityManager = $this->getDoctrine()->getManager();
        $orderType = $entityManager->getRepository(OrderType::class)->findOneBy(["code"=>'express']);
        $order->setOrderType($orderType);
        $price=0;
        /** @var Order $order */
        if($order->getOrderType()->getCode() == 'express') {
            //  $order->setUser($this->user);
            $Country_r = $this->getDoctrine()->getRepository(Country::class);
            list($From,$To) = $Country_r->getShortNameCountry($this->my_address['country'],$order->getAddresses()->getCountry()->getId());
            $One_order = $order;
            $dhlSendBoxAddress =[];
            $dhlSendBoxAddress['from']=$From;
            $dhlSendBoxAddress['to']=$To;
            $Dlh = new DhlDeliveryService($dhlSendBoxAddress,$entityManager);
            $FinalPrice = $Dlh->getDHLPrice($One_order,$this->isVip);

            if(!$FinalPrice){
                $this->addFlash('errors','Вы превысили допустимое значения!');
                return $this->redirectToRoute('post_parcels_create');
            }
            $price=$FinalPrice;
        }
        else{
            $weight=max($order->getSendDetailWeight(),$order->getVolumeWeigth());


            $ObjectPrice = $this->getDoctrine()
                ->getRepository(PriceForDeliveryType::class)
                ->findPriceByWeight($weight,$order->getOrderType()->getId());
            if ($ObjectPrice){
            ($this->isVip)?
                $price=$ObjectPrice->getVipPrice()
                :
                $price=$ObjectPrice->getPrice();
            }else $price='-';
        }
        return  $price;
    }
    /**
     * @Route("/{id}/edit", name="post_parcels_edit")
     */
    public function parcelsEditAction(Request $request, invoicesService $invoicesService,TranslatorInterface $translateService): Response
    {
        $this->getTemplateData();
        $entityManager = $this->getDoctrine()->getManager();
        $errors =[];
        $this->optionToTemplate['page_id']='post_parcels';
        $this->optionToTemplate['page_title']='Order Edit';
        $id = $request->get('id',false);
        if ($id && (int)$id>0){
            /** @var Order $order */
            $order =$entityManager->getRepository(Order::class)->find((int)$id);
            if(empty($order) || $order->getUser()!=$this->getUser()){
                throw new ServiceException('Not found');
            }

        }
        /* @var $order Order */
        $originalProducts = new ArrayCollection();

        // Создать ArrayCollection текущих объектов Tag в БД
        foreach ($order->getProducts() as $product) {
            $originalProducts->add($product);
        }


        $originalCount=$originalProducts->count();
        $orderForm=$request->request->get('order_form',false);
        if ($orderForm)
        {
            if ($products=$orderForm['products']??false){

                $count=count($products) - $originalCount;

                if ($count>0){
                    for ($x=0; $x<=$count; $x++){
                        $orderProduct=new OrderProducts();
                        $order->addProduct($orderProduct);
                    }
                }

            }
        }

        //$address = new Address();
/*
        $maxWeightEconom = $this->getDoctrine()
            ->getRepository(PriceForDeliveryType::class)
            ->findMaxWeight();
        $maxWeightEconomVip = $this->getDoctrine()
            ->getRepository(PriceForDeliveryType::class)
            ->findMaxWeight();
*/
        $maxWeightEconom =$maxWeightEconomVip = 1500;
        $form = $this->createForm(OrderFormType::class, $order, ['attr'=>['user' => $this->user, 'maxWeightEconom' => $maxWeightEconom, 'maxWeightEconomVip' => $maxWeightEconomVip]]);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
            $this->useCoupon=$this->getDiscountCoupon($data,true);
            $this->isVip=($this->useCoupon || $this->user->isVip())?true:false;

            $declareValue=0;
            if ($order->getProducts()){
                /** @var OrderProducts $product */
                foreach ($order->getProducts() as $product){

                    if (empty($product->getDescEn())) {
                        $order->removeProduct($product);
                        $entityManager->remove($product);
                        $entityManager->persist($order);
                    } else {
                        $product->setOrderId($order);
                        $url = Arr::get($product->getOgInfo(),'url',null);
                        if ($url && strpos($url,$product->getDescEn())===false){
                            $product->setOgInfo([]);
                        }
                        $entityManager->persist($product);
                        $declareValue=$declareValue+$product->getCount()*$product->getPrice();
                    }
                }
            }
            $order->setDeclareValue($declareValue);
            unset($product);

            list($shipCost,$volume)=$this->CalculateShipCost($order);
           // $order->setShippingCosts($shipCost);
            $order->setVolumeWeigth($volume);

//Calculate shipping costs for ECONOM type . Use price-weight data.

//Calculate shipping costs for EXPRESS type . Use Dhl service .



            foreach ($originalProducts as $product) {
                if (false === $order->getProducts()->contains($product)) {
                    $entityManager->remove($product);
                }
            }
            $order->setOrderType($this->getOrderTypeFromRequest($request));

            $orderCost=$this->getOrderPrice($order,$orderForm);

            $order->setShippingCosts($orderCost);
            $order->setOrderShippingCosts(null);

            $entityManager->persist($order);
            $entityManager->flush();

            $invoicesService->createInvoice($order,false);

            if ($order->getShippingCosts()>0)  $invoicesService->createInvoice($order,true);

            $this->addFlash(
                'success',
                $translateService->trans("Orders Update sucusfull")
            );
            return $this->redirectToRoute('post_parcels');
        }elseif ($form->isSubmitted() && !$form->isValid()){
            $errors = $form->getErrors(true);
        }
        $twigoption = array_merge($this->optionToTemplate,['form' => $form->createView(),
            'useCoupon'=>$order->isUseCoupon(),
            "calculate"=>$order->isCalculate(),
            "economPrice"=>$order->getEconomPrice(),
            "expressPrice"=>$order->getExpressPrice(),
            "deliveryType"=>($order->getOrderType())?$order->getOrderType()->getCode():null,
            'error' => $errors
            ]
        );

        return $this->render('cabinet/parcels/editform.html.twig', $twigoption);

    }

    private function CalculateShipCost( $object )
    {
        $entityManager = $this->getDoctrine()->getManager();
        $resReturn=0;
        $volume=0;
        /* @var $object Order */
        $weight=(float)$object->getSendDetailWeight();
        $s1=(float)$object->getSendDetailWidth();
        $s2=(float)$object->getSendDetailHeight();
        $s3=(float)$object->getSendDetailLength();
        $volume=round($s1*$s2*$s3/5,3);
        $resW=max($weight,$volume);

        return [$resW,$volume];
    }

    /**
     * @Route("post/parcels/supprort",name="parcles_support")
     */
    public function parclesSupport(Request $request,\Swift_Mailer $mailer)
    {
        $this->getTemplateData();
        $form = $this->createForm(SupportType::class, null, ['attr' => ['user' => $this->user]]);
        $form->handleRequest($request);
        $errors = [];


        if ($form->isSubmitted()  && $form->isValid()) {
            $user= $this->user;
            $template = $this->render('cabinet/support/SupportFormMessage.html.twig');
            $message = (new \Swift_Message('Hello Email'))
                ->setFrom('send@example.com')
                ->setTo('recipient@example.com')
                ->setBody(html_entity_decode($template),'text/html');


            $mailer->send($message);

            $this->addFlash('modal_window',"true");

            return $this->redirectToRoute('parcles_support');
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $errors = $form->getErrors(true);

        }
        $twigoption = array_merge($this->optionToTemplate, ['SupportForm' => $form->createView(),
            'error' => $errors]);
        return $this->render('cabinet/support/support_form.html.twig', $twigoption);
    }

//    /**
//     * @Route("/{id}/sendtosklad", name="post_sendtosklad")
//     */
//    public function parclesSendToSklad(Request $request)
//    {
//        $entityManager = $this->getDoctrine()->getManager();
//        $id = $request->get('id',false);
//        if ($id && (int)$id>0) {
//            $order = $entityManager->getRepository(Order::class)->find((int)$id);
//            if (empty($order) || $order->getUser() != $this->getUser()) {
//               // throw new ServiceException('Not found');
//            }
//        }
//
//        if(!empty($order->getOrderStatus())&&($order->getOrderStatus()->getStatus() == 'paid')){
//            $service = new SkladUsaService();
//            $result = $service->sendOrderToSklad($order);
//            if(json_decode($result)->status == 'success') {
//                $orderStatus = $entityManager->getRepository(OrderStatus::class)->findOneBy(['status' => 'complit']);
//                $order->setOrderStatus($orderStatus);
//                $entityManager->persist($order);
//                $entityManager->flush();
//            }
//        }
//
//        $referer = $request->headers->get('referer');
//        return $this->redirect($referer);
//    }

    /**
     * @Route("/ajax/dhl/price", name="dhl_price")
     * @param Request $request
     */
    public function ajaxDhlPrice(Request $request)
    {

            if(!empty($request->query->get('Height'))
            && !empty($request->query->get('Length'))
            && !empty($request->query->get('Weight'))
            && !empty($request->query->get('Width'))){
/** @var EntityManager $entityManager */
                $entityManager = $this->getDoctrine()->getManager();
                $Vip =$request->query->get('Vip');
        $Country_r = $this->getDoctrine()->getRepository(Country::class);
        $Adress_r = $this->getDoctrine()->getRepository(Address::class);
        $order = new Order();
        $this->getUser();
        $this->getTemplateData();
                $Adress = $Adress_r->find($request->query->get('id_adress'));
                //$From = $Country_r->findOneBy(['name' => $this->my_address['country']]);
                $To = $Country_r->findOneBy(['id' => $Adress->getCountry()->getId()]);
                //$From = $From->getShortName();
                //$To = $To->getShortName();
                //$dhlSendBoxAddress = $this->my_address;
                $dhlSendBoxAddress = [];
               // $dhlSendBoxAddress['from'] = $From;
                $dhlSendBoxAddress['to'] = $To->getShortName();
                $order->setSendDetailHeight($request->query->get('Height'));
                $order->setSendDetailLength($request->query->get('Length'));
                $order->setSendDetailWeight($request->query->get('Weight'));
                $order->setSendDetailWidth($request->query->get('Width'));
                $order->setUser($this->getUser());
                $order->setAddresses($Adress);

                $deliveryType=$request->query->get('deliveryType',false);
                if ($deliveryType){
                    /** @var OrderType $deliveryType */
                    $deliveryType=$entityManager->getRepository(OrderType::class)->find($deliveryType);
                    $typeId=$deliveryType->getId();
                    $pricetype=$deliveryType->getPricetype();
                    $order->setOrderType($deliveryType);
                }


                $order->setCoupon($request->query->get('cupon',null));

                    $this->useCoupon=$this->getDiscountCoupon($order);
                    $this->isVip=($this->useCoupon || $Vip)?true:false;

                list($shipCost,$volume)=$this->CalculateShipCost($order);
                // $order->setShippingCosts($shipCost);
                $order->setVolumeWeigth($volume);

                $weightPrice=$this->getOrderPrice($order,['sendDetailWeight'=>$order->getSendDetailWeight()]);


                return new JsonResponse(['price'=>$weightPrice,'cupon'=>$this->couponData]);
            }

        return new JsonResponse(['price'=>'error','cupon'=>$this->couponData]);
    }

    /**
     * @Route("/ajax/max/price", name="max_price")
     * @param Request $request
     */
    public function ajaxMaxPriceandWeight(Request $request){
      $typeDelivery =   $request->query->get('typeDelivery');
        $results = $this->getDoctrine()->getRepository(PriceForDeliveryType::class) ->findMaxWeight($typeDelivery);
       $data = array('MaxVipPrice'=>(string)$results[3],'MaxPrice'=>(string)$results[2],'MaxWeight'=>(string)$results[1]);
        return new JsonResponse($data);
        }

    /**
     * @Route("/ajax/get-cource/uah", name="ajax-get-cource-uah")
     * @param Request $request
     */
    public function ajaxGetCourceUah(Request $request,ConvertCurrencyPrivatBankService $convertCurrencyPrivatBankService){

        return new JsonResponse($convertCurrencyPrivatBankService->convertToUsdFromCur(1,"UAH", date("d.m.Y")));
    }




}

