<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, \Swift_Mailer $mailer): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);
//dd($form->isSubmitted(), $form->isValid(), $form->getErrors(), $request);
        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $user->setRoles($user::DEFAULT_ROLES);
            $entityManager = $this->getDoctrine()->getManager();
            $user
                ->setEmailVerifyCode($this->generateVerifyEmailCode($user))
                ->setEmailVerify(false)
            ;

            $entityManager->persist($user);
            $entityManager->flush();

            $template = $this->render('Email/verify_email.html.twig');
            $template = str_replace("{{name}}", $user->getUsername(), $template);
            $template = str_replace("{{url}}", $this->generateUrl("post_email_verify_code",["code"=>$user->getEmailVerifyCode()],UrlGeneratorInterface::ABSOLUTE_URL), $template);
            $message = (new \Swift_Message('Email verify in site expressposhta.com'));
            $message      ->setSubject('Email verify in  site expressposhta.com')
                ->setFrom('admin@expressposhta.com')
                ->setTo($user->getEmail())
                ->setBody(html_entity_decode($template),'text/html');

            $mailer->send($message);

            // do anything else you need here, like send an email

            return $this->redirectToRoute('post_email_verify');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),

        ]);
    }

    /**
     * @param User $user
     * @return string
     */
    protected function generateVerifyEmailCode(User $user): string
    {
        return md5("expresspochta".$user->getEmail()."|".time());
    }
}
