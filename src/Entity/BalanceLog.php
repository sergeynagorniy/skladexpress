<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity()
 *
 * @ORM\Table(name="user_balance_log_tail", indexes={@Index(name="user_balance_log_tail_process", columns={"process"}),@Index(name="user_balance_log_tail_created_at", columns={"created_at"})})
 * @ORM\HasLifecycleCallbacks()
 */
class BalanceLog
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var
     *
     * @ORM\Column(name="balance", type="float", nullable=true)
     */
    private $balance;

    /**
     * @var
     *
     * @ORM\Column(name="usrer_balance", type="float", nullable=true)
     */
    private $userBalance;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=512)
     */
    protected $text;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", length=512)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="route", type="string", length=512, nullable=true)
     */
    protected $route;


    /**
     * @var integer
     *
     * @ORM\Column(name="el_id", type="integer", length=512, nullable=true)
     */
    protected $elId;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $user;



    /**
     * @var
     * @ORM\Column(name="process", type="boolean", nullable=true, options={"default":false})
     */
    private $process;


    /**
     * @var
     * @ORM\Column(name="close", type="boolean", nullable=true, options={"default":false})
     */
    private $close;

    /**
     * @var string
     *
     * @ORM\Column(name="created_at", type="datetime", length=512, nullable=true)
     */
    private $createdAt;

        public function __construct() {
            $this->createdAt = new \DateTime();
            $this->process   = false;
            $this->close     = false;
        }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Account
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
//        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set balance
     *
     * @param float $balance
     * @return $this
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return float 
     */
    public function getBalance()
    {
        return round($this->balance,2);
        //return $this->balance;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     * @return $this
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set route
     *
     * @param string $route
     * @return $this
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route
     *
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }


    /**
     * Get route
     *
     * @return string
     */
    public function getRouteName()
    {
        return $this->route;
    }


    /**
     * Set elId
     *
     * @param integer $elId
     * @return $this
     */
    public function setElId($elId)
    {
        $this->elId = $elId;

        return $this;
    }

    /**
     * Get elId
     *
     * @return integer
     */
    public function getElId()
    {
        return $this->elId;
    }


    /**
     * Set process
     *
     * @param boolean $process
     * @return $this
     */
    public function setProcess($process)
    {
        $this->process = $process;

        return $this;
    }

    /**
     * is process
     *
     * @return boolean
     */
    public function isProcess()
    {
        return $this->process ?? false;
    }

    /**
     * Set close
     *
     * @param boolean $close
     * @return $this
     */
    public function setClose($close)
    {
        $this->close = $close;

        return $this;
    }

    /**
     * is close
     *
     * @return boolean
     */
    public function isClose()
    {
        return $this->close ?? false;
    }

    /**
     * Set userBalance
     *
     * @param float $userBalance
     * @return $this
     */
    public function setUserBalance($userBalance)
    {
        $this->userBalance = $userBalance;
    
        return $this;
    }

    /**
     * Get userBalance
     *
     * @return float 
     */
    public function getUserBalance()
    {
        return round($this->userBalance,2);
    }

    public function getTrsString(){
        return $this->getElId();
    }
}
