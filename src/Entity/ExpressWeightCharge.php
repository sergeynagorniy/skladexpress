<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExpressWeightChargeRepository")
 */
class ExpressWeightCharge
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var OrderType
     * @ORM\ManyToOne(targetEntity="OrderType", inversedBy="pricetype")
     * @ORM\JoinColumn(name="order_type", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */

    private $ordertype;

    /**
     * @ORM\Column(type="float")
     */
    private $max_weight;

    /**
     * @ORM\Column(type="float")
     */
    private $chargeNormal;

    /**
     * @ORM\Column(type="float")
     */
    private $chargeVip;




    public function getId(): ?int
    {
        return $this->id;
    }

    public function setOrdertype($ordertype)
    {
        $this->ordertype = $ordertype;

        return $this;
    }


    public function getOrdertype()
    {
        return $this->ordertype;
    }

    public function getMaxWeight(): ?float
    {
        return $this->max_weight;
    }

    public function setMaxWeight(float $max_weight): self
    {
        $this->max_weight = $max_weight;

        return $this;
    }

    public function getChargeNormal(): ?float
    {
        return $this->chargeNormal;
    }

    public function setChargeNormal(float $chargeNormal): self
    {
        $this->chargeNormal = $chargeNormal;

        return $this;
    }

    public function getChargeVip(): ?float
    {
        return $this->chargeVip;
    }

    public function setChargeVip(float $chargeVip): self
    {
        $this->chargeVip = $chargeVip;

        return $this;
    }
}
