<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use App\Entity\Order;
use App\Entity\User;
use App\Entity\Address;
use App\Entity\Transaction;
/**
 * Invoices
 *
 * @ORM\Table(name="invoices")
 * @ORM\Entity
 */
class Invoices
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="addresses")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */

    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;


    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;

    /**
     * @var Order
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="invoices")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */

    private $orderId;

    /**
     * @ORM\OneToMany(targetEntity="Transaction", mappedBy="invoice")
     *
     */
    private $transactions;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_paid", type="boolean")
     */
    private $isPaid = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="try_paid", type="boolean")
     */
    private $tryPaid = false;

    /**
     * @var string
     * @ORM\Column(name="form_token", type="string", length=4096, nullable=true)
     */
    public $formToken;

    /**
     * @var boolean
     *
     * @ORM\Column(name="no_comission", type="boolean")
     */

    private $noComission = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="shipping_pay", type="boolean")
     */

    private $shippingPay = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="refund", type="boolean")
     */

    private $refund = false;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function __construct() {
        $this->transactions = new ArrayCollection();
    }

       /**
     * Set order_id
     *
     * @param string $orderId
     * @return $this
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get order_id
     *
     * @return Order
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
    public function isPaid()
    {
        return $this->isPaid;
    }

    public function setIsPaid(bool $isPaid)
    {
        $this->isPaid = $isPaid;

        return $this;
    }


 public function isTryPaid()
    {
        return $this->tryPaid;
    }

    public function setTryPaid(bool $tryPaid)
    {
        $this->tryPaid = $tryPaid;

        return $this;
    }


    /**
     * Set price
     *
     * @param float $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }



    public function __toString() {
        if($this->getId()) {
            return '#' . $this->getId();
        } else {
            return 'New orders add payment';
        }
    }

    public function addTransaction(Transaction $transaction)
    {
        if ($this->transactions->contains($transaction)) {
            return;
        }

        $this->transactions[] = $transaction;
      //  $transaction->setOrder($this);
    }
    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    public function removeTransaction(Transaction $transaction)
    {
        $this->transactions->removeElement($transaction);
        // установите владеющую сторону, как null
       // $transaction->setOrder(null);
    }

    public $order;

    /**
     * @return string
     */
    public function getFormToken()
    {
        return $this->formToken;
    }

    /**
     * @param string $formToken
     * @return $this
     */
    public function setFormToken($formToken)
    {
        $this->formToken = $formToken;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNoComission(): bool
    {
        return $this->noComission;
    }

    /**
     * @param bool $noComission
     * @return $this
     */
    public function setNoComission(bool $noComission)
    {
        $this->noComission = $noComission;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }


    /**
     * @param bool $shippingPay
     * @return $this
     */
    public function setShippingPay(bool $shippingPay)
    {
        $this->shippingPay = $shippingPay;
        return $this;
    }

    /**
     * @return bool
     */
    public function isShippingPay(): bool
    {
        return $this->shippingPay;
    }

    /**
     * @return bool
     */
    public function isRefund(): bool
    {
        return $this->refund;
    }

    /**
     * @param bool $refund
     * @return $this
     */
    public function setRefund(bool $refund)
    {
        $this->refund = $refund;
        return $this;
    }

    public function getRouteName()
    {
        return "invoice";
    }
}
