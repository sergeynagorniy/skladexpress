<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * OrderProducts
 *
 * @ORM\Table(name="order_products", indexes={@Index(name="torder_products_desc_en", columns={"desc_en"},options={"length":512}) } )
 * @ORM\Entity
 */
class OrderProducts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_en", type="string", length=512, nullable=true)
     */
    private $descEn;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_ua", type="text", nullable=true)
     */
    private $descUa;

    /**
     * @var float
     *
     * @Assert\NotNull
     * @Assert\GreaterThan(0)
     *
     * @ORM\Column(name="price_grn", type="float", nullable=true)
     */
    private $priceGrn;

    /**
     * @var float
     *
     * @Assert\NotNull
     * @Assert\GreaterThan(0)
     *
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;

    /**
     * @var integer
     *
     * @Assert\NotNull
     * @Assert\GreaterThan(0)
     * @ORM\Column(name="count", type="integer", nullable=true)
     */
    private $count;

    /**
     * @var float
     *
     * @ORM\Column(name="total_summ", type="float", nullable=true)
     */
    private $totalSumm;

    /**
     * @var Order
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="products", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */

    private $orderId;

    /**
     * tracking number for user
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=512, nullable=true)
     */

    public $trNum;

    /**
     * @var string
     *
     * @ORM\Column(name="og_info", type="text", nullable=true)
     */
    private $ogInfo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set count
     *
     * @param integer $count
     * @return OrderProducts
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer 
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set order_id
     *
     * @param string $orderId
     * @return OrderProducts
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get order_id
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set descEn
     *
     * @param string $descEn
     * @return OrderProducts
     */
    public function setDescEn($descEn)
    {
        $this->descEn = $descEn;

        return $this;
    }

    /**
     * Get descEn
     *
     * @return string
     */
    public function getDescEn()
    {
        return $this->descEn;
    }

    /**
     * Set descUa
     *
     * @param string $descUa
     * @return OrderProducts
     */
    public function setDescUa($descUa)
    {
        $this->descUa = $descUa;

        return $this;
    }

    /**
     * Get descUa
     *
     * @return string
     */
    public function getDescUa()
    {
        return $this->descUa;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return OrderProducts
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set totalSumm
     *
     * @param float $totalSumm
     * @return OrderProducts
     */
    public function setTotalSumm($totalSumm)
    {
        $this->totalSumm = $totalSumm;

        return $this;
    }

    /**
     * Get totalSumm
     *
     * @return float
     */
    public function getTotalSumm()
    {
        $price =$this->getPrice() ?? 0;
        $count = $this->getCount() ?? 0;

        return $price*$count;
    }

    public function __toString() {
        if($this->getId()) {
            return '#' . $this->getId();
        } else {
            return 'New orders product';
        }
    }

    /**
     * @return float
     */
    public function getPriceGrn()
    {
        return $this->priceGrn??0;
    }

    /**
     * @param float $priceGrn
     */
    public function setPriceGrn(float $priceGrn)
    {
        $this->priceGrn = $priceGrn;

        return $this;
    }

    /**
     * @return string
     */
    public function getTrNum()
    {
        return $this->trNum;
    }

    /**
     * @param string $trNum
     */
    public function setTrNum($trNum)
    {
        $this->trNum = $trNum;

        return $this;
    }

    /**
     * @return array
     */
    public function getOgInfo()
    {
        try {
            $returnArr = json_decode($this->ogInfo,true);
        }
        catch (\Exception $exception){
            $returnArr = [];
        }
        return $returnArr;
    }

    /**
     * @param mixed $ogInfo
     * @return $this
     */
    public function setOgInfo($ogInfo)
    {
        if (is_array($ogInfo)){
            $this->ogInfo = json_encode($ogInfo);
        }else $this->ogInfo = $ogInfo;

        return $this;
    }
}
