<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Order;
use App\Entity\User;
use App\Entity\Address;
use App\Entity\Invoices;

/**
 * @ORM\Table(name="transaction_liq_pay")
 * @ORM\Entity(repositoryClass="App\Repository\TransactionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Transaction
{

    const TRANSACTION_PAYPAL = 2;
    const TRANSACTION_AUTHORIZENET = 1;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=512, nullable=true)
     */
    private $number;

    /**
     * @var float
     *
     * @ORM\Column(name="sum", type="float", nullable=true)
     */
    private $sum;

    /**
     * @var float
     *
     * @ORM\Column(name="total_sum", type="float",  nullable=true)
     */
    private $totalSum;

    /**
     * @var float
     *
     * @ORM\Column(name="comission_sum", type="float",  nullable=true)
     */
    private $comission;

    /**
     * @var float
     *
     * @ORM\Column(name="per_tr_fee", type="float",  nullable=true)
     */
    private $perTransactionFee;


    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=512, nullable=true)
     */
    protected $status;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=512, nullable=true)
     */
    protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=512, nullable=true)
     */
    protected $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=512, nullable=true)
     */
    protected $phoneNumber;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="User", inversedBy="transaction")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Invoices",inversedBy="transactions")
     * @ORM\JoinColumn(name="invoice", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $invoice;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     * @ORM\Column(name="liqpay_order_id", type="string", length=512, nullable=true)
     */
    private $liqpayOrderId;

    /**
     * @var string
     *
     * @ORM\Column(name="liqpay_info", type="text", nullable=true)
     */
    private $payInfo;

    /**
     * Set number
     *
     * @param string $number
     * @return Transaction
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_system", type="integer", options={"default" : 1})
     */

    private $paymentSystem;

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Transaction
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set invoice
     *
     * @param Invoices $invoice
     * @return Transaction
     */
    public function setInvoice(Invoices $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return Invoices
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->paymentSystem = self::TRANSACTION_AUTHORIZENET;
    }

    public function __toString()
    {
        return $this->getNumber();
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Transaction
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

     /**
     * Set lastName
     *
     * @param string $lastName
     * @return Transaction
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getFullName()
    {
       // return $this->firstName." ".$this->middleName." ".$this->lastName;
        return $this->lastName." ".$this->middleName;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     * @return Transaction
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string 
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Transaction
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public $createdAtStr;
    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAtStr()
    {
        return $this->createdAt->format('Y-m-d H:i:s');
    }

    public function setLiqpayOrderId($liqpayOrderId)
    {
        $this->liqpayOrderId = $liqpayOrderId;

        return $this;
    }


    public function getLiqpayOrderId()
    {
        return $this->liqpayOrderId;
    }



    public function setSum($sum)
    {
        $this->sum = $sum;

        return $this;
    }

    public function getSum()
    {
        return $this->sum;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setPayInfo($payInfo)
    {
        $this->payInfo = $payInfo;

        return $this;
    }

    public function getPayInfo()
    {
        return $this->payInfo;
    }

    public function getRouteName()
    {
        return "transaction";
    }

    /**
     * @return string
     */
    public function getComission()
    {
        return $this->comission;
    }

    /**
     * @param float $comission
     * @return $this
     */
    public function setComission(float $comission)
    {
        $this->comission = $comission;

        return $this;
    }

    /**
     * @return float
     */
    public function getPerTransactionFee(): float
    {
        return $this->perTransactionFee;
    }

    /**
     * @param float $perTransactionFee
     * @return $this
     */
    public function setPerTransactionFee(float $perTransactionFee)
    {
        $this->perTransactionFee = $perTransactionFee;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalSum(): float
    {
        return $this->totalSum;
    }

    /**
     * @param float $totalSum
     * @return $this
     */
    public function setTotalSum(float $totalSum)
    {
        $this->totalSum = $totalSum;

        return $this;
    }

    /**
     * @return int
     */
    public function getPaymentSystem(): int
    {
        return $this->paymentSystem;
    }

    /**
     * @param int $paymentSystem
     * @return $this
     */
    public function setPaymentSystem($paymentSystem)
    {
        $this->paymentSystem = $paymentSystem;

        return $this;
    }
}
