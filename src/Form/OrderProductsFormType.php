<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\OrderProducts;
use App\Entity\Address;

//use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Length;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class OrderProductsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descEn', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'data-name'   => 'descEn',
//                    'placeholder' => 'descEn',
                    'autocomplete' => 'off'],
                'label' => 'descEn',
                'required' => true
            ])
//            ->add('descUa', null, [
//                'attr' => [
//                    'class' => 'form-control',
//                    'data-name'   => 'descUa',
////                    'placeholder' => 'descUa',
//                    'autocomplete' => 'off'],
//                    'label' => 'descUa',
//                    'required' => true
//            ])
            ->add('priceGrn', null, [
                'attr' => [
                    'class' => 'form-control order_form_product_price_grn',
                    'data-name'   => 'pricegrn',
//                    'placeholder' => 'price',
                    'autocomplete' => 'off'],
                'label' => 'price',
                'required' => false,
                'constraints' => array(
                    new NotBlank(),
                    new GreaterThan(0),
                )
            ])
            ->add('price', null, [
                'attr' => [
                    'class' => 'form-control order_form_product_price',
                    'data-name'   => 'price',
                    'readOnly' => 'readOnly',
//                    'placeholder' => 'price',
                    'autocomplete' => 'off'],
                'label' => 'price',
                'required' => false,
                'constraints' => array(
                    new NotBlank(),
                    new GreaterThan(0),
                )
            ])
            ->add('count', TextType::class, [
                'attr' => [
                    'class' => 'form-control order_form_product_count',
                    'data-name'   => 'count',
//                    'placeholder' => 'count_short',
                    'autocomplete' => 'off'],
                'label' => 'count',
                'required' => false,
                'constraints' => array(
                    new NotBlank(),
                    new GreaterThan(0),
                )
            ])
            ->add('totalSumm', null, [
                'attr' => [
                    'class' => 'form-control order_form_product_total',
                    'data-name'   => 'total',
                    'readOnly' => 'readOnly',
//                    'placeholder' => 'price',
                    'autocomplete' => 'off'],
                'label' => 'total',
                'required' => false
            ])
            ->add('orderId', HiddenType::class,array(
                'data' => $options['orderId'],
                'required'          => false
            ));
       // ->add('save', SubmitType::class, [
       //                         'attr' => ['class' => 'btn btn-block btn-primary mt-3'],
       //                         ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderProducts::class,
            'orderId' => null
        ]);
    }

    public function getBlockPrefix()
    {
        return 'OrderProductsFormType';
    }
}
