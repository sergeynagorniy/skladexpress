<?php

namespace App\Repository;


use App\Entity\Balance;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Balance|null find($id, $lockMode = null, $lockVersion = null)
 * @method Balance|null findOneBy(array $criteria, array $orderBy = null)
 * @method Balance[]    findAll()
 * @method Balance[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BalanceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Balance::class);
    }


    public function getNewBalances($user_id,$maxResult=5)
    {
        $qr = $this->createQueryBuilder('o')
            ->where('o.user = :user_id')
            ->setParameter('user_id', $user_id)
            ->setMaxResults($maxResult)
            ->orderBy('o.createdAt','DESC')
        ;
        return $qr->getQuery()->getResult();
    }

    public function getBalanceHistory($user_id)
    {
        $qr = $this->createQueryBuilder('o')
            ->where('o.user = :user_id')
            ->setParameter('user_id', $user_id)
        ->orderBy("o.createdAt","DESC");
        return $qr->getQuery()->getResult();
    }

}
