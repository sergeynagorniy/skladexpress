<?php

namespace App\Repository;

use App\Entity\ExpressWeightCharge;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExpressWeightCharge|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExpressWeightCharge|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExpressWeightCharge[]    findAll()
 * @method ExpressWeightCharge[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExpressWeightChargeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExpressWeightCharge::class);
    }

    // /**
    //  * @return ExpressWeightCharge[] Returns an array of ExpressWeightCharge objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExpressWeightCharge
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findChargeByWeight(float $weight)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.max_weight >= :DetailWeight')
            ->setParameter('DetailWeight', $weight)
            ->setMaxResults(1)
            ->orderBy('p.max_weight','ASC')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
