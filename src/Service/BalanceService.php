<?php

namespace App\Service;

use App\Entity\BalanceLog;
use App\Entity\FormHelps;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;

use Twig\Environment;

class BalanceService
{
    const BALANCE_OPERATION_TYPE_PLUSS = 1;
    const BALANCE_OPERATION_TYPE_MINUSS = 2;
    /** @var EntityManagerInterface  */
    private $entityManager;

    private $twig;

    public function __construct(
        EntityManagerInterface $entityManager,
        Environment $twig,
        EntityManagerInterface $em
        )
    {
        $this->entityManager = $entityManager;
        $this->twig = $twig;
        $this->em = $em;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param User            $user
     * @param float           $sum
     * @param string          $text
     * @param \DateTime | null $date
     *
     * @return bool
     */
    public function addMoney(User $user, $sum, $text, $date = null,$object=null)
    {
        if($user == null) {
            return false;
        }
        $balanceEntity = new \App\Entity\BalanceLog();
        $balanceEntity->setBalance($sum)
            ->setUserBalance($user->getBalance())
                ->setUser($user)
                ->setText($text)
                ->setType(self::BALANCE_OPERATION_TYPE_PLUSS)
                ->setProcess(false)
                ->setClose(false);
        if($date == null){
            $balanceEntity->setCreatedAt(new \DateTime());
        } else {
            $balanceEntity->setCreatedAt($date);
        }

        if(!empty($object)){
            if (method_exists($object,'getRouteName')) {
                $balanceEntity->setRoute($object->getRouteName());
            }

            if (method_exists($object,'getId')) {
                $balanceEntity->setElId($object->getId());
            }
        }

        $this->getEm()->persist($balanceEntity);
        $this->getEm()->flush([$balanceEntity]);
        return true;
    }


    /**
     * @param User            $user
     * @param float           $sum
     * @param string          $text
     * @param \DateTime | null $date
     *
     * @return bool
     */
    public function minusMoney(User $user, $sum, $text, $date = null,$object=null)
    {
        if($user == null) {
            return false;
        }
        $balanceEntity = new \App\Entity\BalanceLog();
        $balanceEntity->setBalance($sum)
            ->setUserBalance($user->getBalance())
            ->setUser($user)
            ->setText($text)
            ->setType(self::BALANCE_OPERATION_TYPE_MINUSS)
            ->setProcess(false)
            ->setClose(false);
        if($date == null){
            $balanceEntity->setCreatedAt(new \DateTime());
        } else {
            $balanceEntity->setCreatedAt($date);
        }

        if(!empty($object)){
            if (method_exists($object,'getRouteName')) {
                $balanceEntity->setRoute($object->getRouteName());
            }

            if (method_exists($object,'getId')) {
                $balanceEntity->setElId($object->getId());
            }
        }

        $this->getEm()->persist($balanceEntity);
        $this->getEm()->flush([$balanceEntity]);
        return true;
    }


    /**
     * @param BalanceLog $object
     * @return bool
     */
    public function moneyFromLog($object)
    {
        /** @var User $user */
        $user = $object->getUser();
        if (!$user || $object->getBalance()==0 )  return false;

            $balance = $user->getBalance();
            if ($object->getType() == self::BALANCE_OPERATION_TYPE_PLUSS) $balance += $object->getBalance();
            else $balance -= $object->getBalance();
            $user->setBalance($balance);

            $balanceEntity = new \App\Entity\Balance();
            $balanceEntity
                ->setBalance($object->getBalance())
                ->setUserBalance($balance)
                ->setUser($user)
                ->setText($object->getText())
                ->setType($object->getType())
                ->setCreatedAt(new \DateTime())
                ->setRoute($object->getRoute())
                ->setElId($object->getElId());


            $this->getEm()->persist($balanceEntity);
            $this->getEm()->flush([$balanceEntity,$user]);
            return true;

    }
}
