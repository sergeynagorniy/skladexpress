<?php
namespace App\Service;



use App\Entity\User as User;
use Doctrine\ORM\EntityManager;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class ConvertCurrencyPrivatBankService
 */
class ConvertCurrencyPrivatBankService
{

    static $defaultCource = [
        'AZN' => 0.5918,
        'BYN' => 0.3917,
        'CAD' => 0.7851,
        'CHF' => 1.0929,
        'CNY' => 0.1537,
        'CZK' => 0.0395,
        'DKK' => 0.1638,
        'EUR' => 1.2086,
        'GBP' => 1.2923,
        'GEL' => 0.3056,
        'HUF' => 0.0034,
        'ILS' => 0.3083,
        'JPY' => 0.0097,
        'KZT' => 0.0024,
        'MDL' => 0.0581,
        'NOK' => 0.1148,
        'PLZ' => 0.2563,
        'RUB' => 0.0128,
        'SEK' => 0.1196,
        'SGD' => 0.7523,
        'TMT' => 0.2898,
        'TRY' => 0.1283,
        'UAH' => 0.0356,
        'UZS' => 0.0001
    ];

    private $systemPath = '';
    private $procent=0;

    public function __construct()
    {

        define("PRIVAT_BANK_CACHE_CURRENCY_FILE",getcwd() . "/../courcePrivatBank.cache");
    }

    /**
     * @param $summ float
     * @param $cur string
     *
     * @return float
     */

    public function convertToUsdFromCur($summ,$cur,$date)
    {
        if ($summ && $cur){
            $rates=$this->getCource($cur,$date);
            return ($rates) ? $this->getSumWithProcent($summ,$rates) : $summ;
        }
        return $summ;
    }

    /**
     * @param $summ float
     *
     * @return float
     */

    public function getSumWithProcent($summ,$rates)
    {
        return round(($summ*$rates + ($summ*$rates*$this->procent/100)),2);
    }

    /**
     * @param $cur string
     *
     * @return float
     */

    public function getCource($cur,$date)
    {
        $time = 0;
        $currentArr=[];
        $currentArr = $this->getFromCache();

        if ($currentArr && is_array($currentArr)) {

            $time = $currentArr['time'] ?? 0;
             $curencies = (isset($currentArr[$date])) ? ($currentArr[$date]['curencies'] ?? []) : [];
        }
        if ($time > 0 && time() <= ($time + (24 * 60 * 60)) && !empty($curencies) && isset($curencies[$cur]) && $curencies[$cur] > 0) {
            return $curencies[$cur] ?? (self::$defaultCource[$cur] ?? 1);
        } else {
            if ($currentArr[$date] = $this->getFromApi($date)) {
                $this->saveToCache($currentArr);
                $time = $currentArr['time'] ?? 0;
                $curencies = $currentArr[$date]['curencies'] ?? [];
                if ($time > 0 && time() <= ($time + (24 * 60 * 60))) {
                    return $curencies[$cur] ?? (self::$defaultCource[$cur] ?? 1);
                }
            }
        }
        return self::$defaultCource[$cur] ?? 1;
    }


    /**
     * @return array | boolean
     */

    public function getFromApi($dateFrom = null){
        $ratesArr=[];
        $uahRate=0;
        if (empty($dateFrom)){
            $dateFrom = date("d.m.Y");
        }
        try {
            $XML=simplexml_load_file('https://api.privatbank.ua/p24api/exchange_rates?date='.$dateFrom);
        }
        catch (\Exception $exception){
            $curl = curl_init('https://api.privatbank.ua/p24api/exchange_rates?date='.$dateFrom);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/34.0.1847.116 Chrome/34.0.1847.116 Safari/537.36');
            $result = curl_exec($curl);
            $XML = new \SimpleXMLElement($result);
            curl_close($curl);
        }

        foreach($XML->exchangerate as $rates){

           // $testRate = (float)$rates["saleRate"] ?? (float)$rates["saleRateNB"] ?? 1 ;
//            $saleRate = (float)$rates["saleRate"] ?? 1;
//            $saleRateNB = (float)$rates["saleRateNB"] ?? 1;

            $saleRate = (float)$rates["purchaseRate"] ?? 1;
            $saleRateNB = (float)$rates["purchaseRateNB"] ?? 1;

            $testRate = ($saleRate > 0) ? $saleRate: $saleRateNB;
            $currencyName = (string)$rates["currency"];
            if ($testRate>0 && !empty($currencyName)) {
                if ( $currencyName == "USD") $uahRate = round(1 / $testRate, 4);
                else $ratesArr[$currencyName] = $testRate;
            }
            //saleRateNB
        }
        foreach($ratesArr as $key=>$rate)  $ratesArr[$key]=round($rate*$uahRate,4);
        if (count($ratesArr)>0) $ratesArr["UAH"]=$uahRate;

        return (count($ratesArr)>0) ? ["curencies"=>$ratesArr] : false;
    }


    /**
     * @return array | boolean
     */

    public function getFromCache(){
        try {
            $currentStr = file_get_contents(PRIVAT_BANK_CACHE_CURRENCY_FILE);
            if (!empty($currentStr)) return unserialize($currentStr);
        }
        catch (\Exception $exception){
            return false;
        }
        return false;
    }

    /**
     * @param $currentArray array
     *
     * @return array | boolean
     */
    public function saveToCache($currentArray=[]){
        $currentArray["time"]=time();
        $currentStr=serialize($currentArray);
        file_put_contents(PRIVAT_BANK_CACHE_CURRENCY_FILE,$currentStr);

        return true;
    }

}