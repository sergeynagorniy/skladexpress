<?php


namespace App\Service\PayPalRestApi;


class Address
{
    private $data;

    public function __construct()
    {
        $this->data = [];

        return $this;
    }


    public function setAdminArea($value)
    {
        $this->data["admin_area_1"] = mb_substr(trim($value),0,126);

        return $this;
    }


    public function setAddressLine1($value)
    {
        $this->data["address_line_1"] = mb_substr(trim($value),0,126);

        return $this;
    }

    public function setAddressLine2($value)
    {
        $this->data["address_line_2"] = mb_substr(trim($value),0,126);

        return $this;
    }

    public function setPostalCode($value)
    {
        $this->data["postal_code"] = mb_substr(trim($value),0,126);

        return $this;
    }

    public function setCountryCode($value)
    {
        $this->data["country_code"] = trim($value);

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }
}