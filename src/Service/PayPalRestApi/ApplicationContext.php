<?php


namespace App\Service\PayPalRestApi;


class ApplicationContext
{
    private $data;

    public function __construct($returnUrl="",$cancelUrl = "")
    {
        $this->data = [];
        $this
            ->setReturnUrl($returnUrl)
            ->setCancelUrl($cancelUrl);
        return $this;
    }


    public function setReturnUrl($value)
    {
        $this->data["return_url"] =  mb_substr(trim($value),0,126);

        return $this;
    }

    public function setCancelUrl($value)
    {
        $this->data["cancel_url"] =  mb_substr(trim($value),0,126);

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

}