<?php


namespace App\Service\PayPalRestApi;


class Item
{
    private $data;

    public function __construct()
    {
        $this->data = [];

        return $this;
    }


    public function setName($name)
    {
        $this->data["name"] = mb_substr($name,0,126);

        return $this;
    }


    public function setQuantity($quantity)
    {
        $this->data["quantity"] = $quantity;

        return $this;
    }

    public function setDescription($description)
    {
        $this->data["description"] = mb_substr($description,0,126);

        return $this;
    }

    public function setSku($sku)
    {
        $this->data["sku"] = mb_substr($sku,0,126);;

        return $this;
    }

    public function setUnitAmount($money)
    {
        $this->data["unit_amount"] = $money;

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

}