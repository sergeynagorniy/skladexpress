<?php


namespace App\Service\PayPalRestApi;


class Money
{
    private $data;

    public function __construct($amount,$curency = "USD")
    {
        $this->data = [];
        $this
            ->setCurrencyCode($curency)
            ->setValue($amount);
        return $this;
    }


    public function setCurrencyCode($value = "USD")
    {
        $this->data["currency_code"] = trim($value);

        return $this;
    }


    public function setValue($value)
    {
        $this->data["value"] = trim($value);

        return $this;
    }

    public function addBreakdownTotalSumm($value)
    {
        $this->data["breakdown"]["item_total"] = $value;

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

}