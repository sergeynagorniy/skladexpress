<?php


namespace App\Service\PayPalRestApi;


class PurshaseUnit
{
    private $data;

    public function __construct()
    {
        $this->data = [];

        return $this;
    }


    public function setDescription($value)
    {
        $this->data["description"]= mb_substr(trim($value),0,126);

        return $this;
    }


    public function setAmount($money)
    {
        $this->data["amount"] = $money;

        return $this;
    }

    public function setCustomId($value)
    {
        $this->data["custom_id"] =  mb_substr(trim($value),0,126);

        return $this;
    }

    public function setInvoiceId($value)
    {
        $this->data["invoice_id"] =  mb_substr(trim($value),0,126);

        return $this;
    }

    public function setItems($value)
    {
        $this->data["items"] =  $value;

        return $this;
    }

    public function addItem($value)
    {
        $this->data["items"][] =  $value;

        return $this;
    }

    public function addShipping($value)
    {
        $this->data["shipping"] =  $value;

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }
}