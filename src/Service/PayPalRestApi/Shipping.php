<?php


namespace App\Service\PayPalRestApi;


class Shipping
{
    private $data;

    public function __construct()
    {
        $this->data = [];

        return $this;
    }


    public function setName($value)
    {
        $this->data["name"]= [
            "full_name"=>mb_substr(trim($value),0,299)
            ];

        return $this;
    }


    public function setAddress($value)
    {
        $this->data["address"] = $value;

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }
}