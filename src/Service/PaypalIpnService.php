<?php

namespace App\Service;

use App\Entity\Transaction;
use Doctrine\ORM\EntityManager;




/**
 * Class PaypalIpnService
 * @package App\Service
 */
class PaypalIpnService
{

    private $name = "dynamok_api1.gmail.com";
    private $psw = "3ABD3QNRMZDUS7HW";
    private $signature = "AFcWxV21C7fd0v3bYYYRCpSSRl31AIQO7u2J4C3Y0GF3iS4EtEu1ZHTp";

    private $transactionSave=false;
    private $systemComission=true;

    private $shopName =null;
    private $refUser =null;
    private $shopType = null;

    private $transactionNumber = null;

    private $isTransactionToSave = true;


    private $systemPath='';



    public function __construct()
    {

    }


    public function parseTransaction($transactionId)
    {
        $transaction  = $this->getTransactionPayPalDetails($transactionId);
        $statusData = 'error';
        $status = $transaction['PAYMENTSTATUS'] ?? null;
        if ($status && $status == "Completed") $statusData = "success";
       return [
            "number"=>$transaction["TRANSACTIONID"] ??null,
            "totalSumm"=>$transaction["AMT"] ??0,
            "comission" => $transaction['FEEAMT']??0,
            "pertrfee" =>0,
            "firstName" => $transaction['FIRSTNAME'] ?? '',
            "lastName" => $transaction['LASTNAME'] ?? '',
            "phoneNumber" => $transaction['EMAIL'] ?? '',
            "status" => $statusData,
            "payInfo" => json_encode($transaction),
            "invoiceStr"=>$transaction['CUSTOM'] ?? '',
            "paymentSystem" => Transaction::TRANSACTION_PAYPAL
        ];
    }






    /**
     * @param $transactionId
     * @param bool $sandbox
     * @return array
     */
    public function getTransactionPayPalDetails($transactionId, $sandbox = false)
    {
        $info = 'USER=' . $this->name
            . '&PWD=' . $this->psw
            . '&SIGNATURE=' . $this->signature
            . '&VERSION=204'
            . '&METHOD=GetTransactionDetails'
            . '&TRANSACTIONID=' . $transactionId;

        if ($sandbox) {
            $url = 'https://api-3t.sandbox.paypal.com/nvp';
        } else {
            $url = 'https://api-3t.paypal.com/nvp';
        }
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $info);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POST, 1);

        $result = curl_exec($curl);

        parse_str($result, $result);
        $rez = array();
        foreach ($result as $key => $value) {
            $rez[$key] = $value;
        }
        return $rez;

    }


    /**
     * @param string $testStr
     * @return int
     *
     */
    public function getClearInvoiceId()
    {
        $testStr = $this->data["invoiceStr"]??'';
        $arrTmp = explode("#", $testStr);
        if ($arrTmp[0] == 'EXPRESSINVOICE' && count($arrTmp) >= 2) {
            $invoiceId = $arrTmp[1] ?? null;
            if ($invoiceId) {
                return (int)$invoiceId;
            }
            return $testStr;
        }
    }


    private function checkLotBuyCustomField($numberItem,$custom = null)
    {
        if ($custom) {
            $dataArr = explode(":",$custom);
            $elType = $dataArr[0] ?? null ;
            $elId = $dataArr[1] ?? null ;
            $shopName = $dataArr[2] ?? null ;
            $user = null;
            if ($elType && $elId){
                if ($elType == 'listing') {
                    /** @var \AppBundle\Entity\Listing $listing */
                    $listing = $this->em->getRepository(\AppBundle\Entity\Listing::class)->find($elId);
                    if ($listing){
                        $user = $listing->getUser();
                        $shopName = "From Listing";
                        $listing->setStatus(true);
                        $this->em->persist($listing);
                    }
                }elseif ($elType == 'payPalButton') {
                    /** @var \AppBundle\Entity\PaypalButton $payPalButton */
                    $payPalButton = $this->em->getRepository("AppBundle:PaypalButton")->findOneBy(array('item' => $elId));
                    /* @var $payPalButton PaypalButton*/
                    if (isset($payPalButton)) {
                        $user = $payPalButton->getUser();
                        $shopName = "From payPalButton";
                        $payPalButton->setPaid(true);
                        $this->em->persist($payPalButton);
                    }
                }elseif ($elType == 'paypalAddToBalance') {
                    /** @var \AppBundle\Entity\User $userTmp */
                    $userTmp=$this->em->getRepository("AppBundle:User")->find((int)$elId);
                    if ($userTmp){
                        $user = $userTmp;
                        $shopName = "From dashboard";
                        $this->systemComission = false;
                    }
                }elseif ($elType == 'skladpaysystem') {
                /** @var \AppBundle\Entity\User $user */
                    $user = $this->em->getRepository("AppBundle:User")->getByShopLogin($shopName,'tilda');
                    /** @var \AppBundle\Entity\PaySystemLog $payElLog */
                    $payElLog = $this->em->getRepository(\AppBundle\Entity\PaySystemLog::class)->find($elId);
                    if ($payElLog){
                        $payElLog->setPaid(true)
                        ->setTransactionNum($this->transactionNumber)
                        ;
                        $this->em->persist($payElLog);
                    }
                }
            }

            if ($user && $shopName){
                $this->setShopName($shopName);
                $lot = new Lot;
                $lot->setNumber($numberItem);
                $lot->setCreatedAt(new \DateTime());
                $lot->setUser($user);
                $lot->setType(3);
                $this->em->persist($lot);
                $this->em->flush();
                return $lot;
            }
            return null;
        }
    }

  }
