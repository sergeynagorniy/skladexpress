<?php

namespace App\Service;

use Doctrine\ORM\EntityManager;


define("LOG_PAYPAL_REST", getcwd() . "/../paypal_rest.log");

class PaypalRESTService
{
    private $isSandbox = false;

    const AUTH_DELIMITER        = ':';

    const ERROR_PREFIX          = 'Paypal: ';
    const ERROR_GLUE            = '; ';

    const PRODUCTION_API_BASE   = 'https://api.paypal.com/';

    private $email              = 'skladusa.pp@gmail.com';
    private $clientId           = 'AcEIugKFCqYJnKXcBHY5wJL0Vc_iO8gMyczC4ywdo4Doll52eQHoSNAin_v-_zrLxmUQKe94OciOR5W7';
    private $secret             = 'EB53ruOrZm5ok1_t5GePeqr9BtL9OBEUxw-hAAEnRjcUxQwjoV4bdc1aSi9BTZ0wRFlGDJzVopcZVh0c';

    const SANDBOX_API_BASE      = 'https://api.sandbox.paypal.com/';

    private $sandboxEmail       = 'sb-ighvj5757199@business.example.com';
    private $sandboxClientId    = 'AWGfY7ciYl3Ke7S-R3l_pwT6QwJ7MvWn9uhO_md5pyNzUfwmYJVZe0E1vYcxsBIarb3iTT0o0MCoGwg2';
    private $sandboxSecret      = 'EOZgMEjUuz-mRHLcImYMZA6r-RXPohBfalDitjAWz5madkjYwqBNzwiQV1yPumPx9ikVMv3-do7bLFHW';

    private $apiBase;

    private $accessToken;

    protected $container;
    private $auth;
    private $payPalAuthAssertion; // can be used in some requests, so not present in the "_sendRequest" method

    const PAYPAL_API_GET_ORDER_URL         = 'v2/checkout/orders/';
    const PAYPAL_API_CREATE_ORDER_URL      = 'v2/checkout/orders/';

    public function __construct( ) {


        $this->apiBase  = $this->isSandbox ? self::SANDBOX_API_BASE : self::PRODUCTION_API_BASE;

        $email          = $this->isSandbox ? $this->sandboxEmail    : $this->email;
        $clientId       = $this->isSandbox ? $this->sandboxClientId : $this->clientId;
        $secret         = $this->isSandbox ? $this->sandboxSecret   : $this->secret;

        $this->setAuth($clientId, $secret);
        $this->setPaypalAuthAssertion($email, $clientId);
    }

    private function setAuth($clientId, $secret) {
        $this->auth = urlencode($clientId) . self::AUTH_DELIMITER . urlencode($secret);
    }

    private function setPaypalAuthAssertion($email, $clientId) {

        $joseHeader = base64_encode(json_encode(['alg' => 'none']));
        $payload    = base64_encode(json_encode([
            'iss'   => $clientId,
            'email' => $email,
        ]));
        $signature  = '';//base64_encode('');

        $this->payPalAuthAssertion  = $joseHeader . '.' . $payload . '.' . $signature;
    }

    public function getAccessToken() {
        $response = $this->_sendRequest(
            'v1/oauth2/token',
            'grant_type=client_credentials',
            true,
            [
                'Accept: application/json',
                'Accept-Language: en_US'
            ],
            [
                CURLOPT_USERPWD => $this->auth
            ],
            true
        );

        $this->accessToken = $response['access_token'] ?? '';
    }



    /**
     * @param $info array Array of transactions parameters: [[transaction_id, tracking_number, status, carrier]]
     * @return mixed
     */
    public function updateTrackingInfo($info) {

//        try {
            $response = $this->_sendRequest(
                'v1/shipping/trackers-batch',
                [
                    'trackers' => $info
                ],
                true
            );

            return $response;
//        } catch (\Exception $e) {
//            echo $e->getTraceAsString();
//        }
    }

    /**
     * @param $transactionPaypalId
     * @param $amount
     * @param $invoiceId
     * @param $noteToPayer
     * @param $paypalRequestId
     * @return mixed
     */
    public function refundPayment(
        $transactionPaypalId,
        $amount             = '',
        $invoiceId          = '',
        $noteToPayer        = '',
        $paypalRequestId    = ''
    ) {

        $headers = [
            "PayPal-Auth-Assertion: {$this->payPalAuthAssertion}"
        ];

        ($paypalRequestId) ? $headers[] = "PayPal-Request-Id: $paypalRequestId" : null;

        $data = [];

        ($amount)       ? $data['amount']           = $amount       : null;
        ($invoiceId)    ? $data['invoice_id']       = $invoiceId    : null;
        ($noteToPayer)  ? $data['note_to_payer']    = $noteToPayer  : null;

        if($this->isSandbox) {
            return $this->refundPaymentTest($transactionPaypalId);
        }

        return $this->_sendRequest(
            "v2/payments/captures/$transactionPaypalId/refund",
            $data,
            true,
            $headers
        );
    }

    private function refundPaymentTest($transactionPaypalId) {

        $testJson = '{
  "id": "' . $transactionPaypalId . '",
  "status": "COMPLETED",
  "links": [
    {
      "rel": "self",
      "method": "GET",
      "href": "https://api.paypal.com/v2/payments/refunds/1JU08902781691411"
    },
    {
      "rel": "up",
      "method": "GET",
      "href": "https://api.paypal.com/v2/payments/captures/2GG279541U471931P"
    }
  ]
}';

        $response = $this->decode($testJson);

        return $response;
    }

    public function getRefundDetails($refundId) {

        if($this->isSandbox) {
            return $this->getRefundDetailsTest($refundId);
        }

        return $this->_sendRequest(
            "v2/payments/refunds/$refundId",
            [],
            false
        );
    }

    private function getRefundDetailsTest($refundId) {

        $testJson = '{
  "id": "1JU08902781691411",
  "amount": {
    "value": "10.99",
    "currency_code": "USD"
  },
  "status": "COMPLETED",
  "note_to_payer": "Defective product",
  "seller_payable_breakdown": {
    "gross_amount": {
      "value": "10.99",
      "currency_code": "USD"
    },
    "paypal_fee": {
      "value": "0",
      "currency_code": "USD"
    },
    "net_amount": {
      "value": "10.99",
      "currency_code": "USD"
    },
    "total_refunded_amount": {
      "value": "10.99",
      "currency_code": "USD"
    }
  },
  "invoice_id": "INVOICE-123",
  "create_time": "2018-09-11T23:24:19Z",
  "update_time": "2018-09-11T23:24:19Z",
  "links": [
    {
      "rel": "self",
      "method": "GET",
      "href": "https://api.paypal.com/v2/payments/refunds/1JU08902781691411"
    },
    {
      "rel": "up",
      "method": "GET",
      "href": "https://api.paypal.com/v2/payments/captures/2GG279541U471931P"
    }
  ]
}';

        $response = $this->decode($testJson);

        return $response;
    }

    private function _sendRequest($route, $data = [], $isPost = false, $headers = [], $curlOptions = [], $isAccessTokenRequest = false, $log = false, $stopOnErrors = true) {

        $defaultHeaders = [
            "Content-Type: application/x-www-form-urlencoded",
            "Accept: application/json",
            "Accept-Encoding: gzip",
        ];

        if( !$isAccessTokenRequest ) {

            if( !$this->accessToken ) {
                $this->getAccessToken();
            }

            unset($defaultHeaders[0]);
            $defaultHeaders = array_merge($defaultHeaders, [
                "Authorization: Bearer {$this->accessToken}",
                "Content-Type: application/json",
            ]);
        }

        $headers = array_merge($defaultHeaders, $headers);

        $curl = curl_init();

        $defaultCurlOptions = [
            CURLOPT_URL             => $this->apiBase . $route . (!$isPost ? '?' . http_build_query($data) : ''),
            CURLOPT_POST            => $isPost,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_HEADER          => false,
            CURLOPT_HTTPHEADER      => $headers,
            CURLOPT_USERAGENT       => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
            CURLOPT_SSL_VERIFYHOST  => 0,
            CURLOPT_SSL_VERIFYPEER  => 0,
            CURLOPT_CONNECTTIMEOUT  => 0,
            CURLOPT_TIMEOUT         => 30,
        ];

        if($isPost) {
            $curlOptions[CURLOPT_POSTFIELDS] = $isAccessTokenRequest ? $data : json_encode($data);
        }

        $curlOptions = $defaultCurlOptions + $curlOptions;

        if($log) {
            $this->log($curlOptions,'PAYPAL REST request: ');
        }

//        $this->pre($curlOptions);

        curl_setopt_array($curl, $curlOptions);

//        if( !$isAccessTokenRequest ) {
//            die();
//        }
        $response = curl_exec($curl);
        if(curl_errno($curl)) {
            throw new \InvalidArgumentException(curl_error($curl));
        }

        $curlOptions = curl_getinfo($curl);

//        $this->pre($curlOptions);

        curl_close($curl);
        if($log) {
            $this->log([], $response);
        }
//        echo $response;
        $response = $this->decode($response);
//        $this->pre($response);

        if($log) {
            $this->log($response,'PAYPAL REST response: ');
        }

        if( $errorsString = trim($this->processAllErrors($response, $curlOptions, $isAccessTokenRequest)) )
            throw new \InvalidArgumentException($errorsString);

        return $response;
    }

    public function pre($array = ['Empty!']) {
        echo '<pre>' . print_r($array, 1) . '</pre>';
    }

    public function decode($response) {

        $result = json_decode($response, true);

        return $result;
    }

    public function log($data = [], $prefix = '') {
        return error_log(
            $prefix . date('Y-m-d H:i') . print_r($data, true). PHP_EOL,
            3,
            LOG_PAYPAL_REST
        );
    }

    public function processAllErrors($response, $curlOptions, $isAccessTokenRequest) {
        return $isAccessTokenRequest ? $this->processAccessTokenErrors($response) : $this->processResponseErrors($response);
    }

    public function processAccessTokenErrors($response) {
        return ($response['error'] ?? '') . ' ' . ($response['error_description'] ?? '');
    }

    public function processResponseErrors($response) {

        $errorsString = '';

        $errors = [];

        $validationErrors = $response['errors'] ?? [];

        foreach($validationErrors as $validationError) {

            $error      = $validationError['name'] . ',' . $validationError['message'] . ': ';
            $details    = $validationError['details'] ?? [];

            foreach ($details as $detail)  {
                foreach($detail as $key => $value) {
                    $error .= $key . ' = "' . $value . '", ';
                }
            }

            $errors[] = $error;
        }

        if( count($errors) )
            $errorsString = self::ERROR_PREFIX . implode(self::ERROR_GLUE . self::ERROR_PREFIX, $errors);

        return $errorsString;
    }


    public function createOrder($purchaseunit,$applicationcontext = null)
    {
         $data=[
            "intent"=> "CAPTURE",
            //"payer" =$payer,
            "purchase_units" => [$purchaseunit]
        ];
         if ($applicationcontext && is_array($applicationcontext)){
             $data["application_context"] = $applicationcontext;
         }
        return $this->_sendRequest(
            self::PAYPAL_API_CREATE_ORDER_URL,
            $data,
            true,
            ["Prefer: return=minimal"]
        );
    }

    public function getOrderDetails($paypalId) {

        return $this->_sendRequest(
            self::PAYPAL_API_GET_ORDER_URL.$paypalId,
            [],
            false
        );
    }
}

?>