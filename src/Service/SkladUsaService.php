<?php

namespace App\Service;

use App\Entity\Order;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;

define("LOG_SKLADUSA", getcwd() . "/../errors-sklad-usa.log");

class SkladUsaService
{

//    protected $api_base_url = 'http://localhost:8080';
    protected $api_base_url = 'https://system.skladusa.com';
//    protected $api_base_url = 'https://test.skladusa.com';
    protected $path_econom = '/api/order_expressposhta_econom/';
    protected $path_express = '/api/order_expressposhta_express/';
    protected $path_calculate = '/api/rate/list';
    protected $api_key = ''; // w

    protected $userId="30";

    protected $userToken="V9dVKudU11vbFbpsRYaDnbhTiEwIbt"; // for userId=30

    // Send Orders data from Expressposhta to Sklad

    public function __construct()
    {
        $this->setApiKey();
    }

    public function setApiKey()
    {
        $this->api_key = base64_encode($this->userId.":".$this->userToken);
    }

    public function sendOrderToSklad(Order $order){

        $data = new ArrayCollection();

        $data->epOrderId = $order->getId();
        //$data->receiverName = $order->getAddresses()->getFullName();
        $data->receiverName = $order->getReceiverName();
        $data->receiverEmail = $order->getEmail();
        $data->receiverAddress = $order->getAddress();
        $data->receiverCity = $order->getCity();
        $data->receiverState = $order->getRegionOblast();
        $data->receiverZip = $order->getZip();
        $data->receiverCountry = $order->getCountryCode();
        //$data->trackingNumber = $order->getTrackingNumber();
        //$data->shippingCompanyToUsa = $order->getCompanySendToUsa();
        //$data->trackingNumberToUsa = $order->getSystemNum();
       // $data->shippingCompanyInUsa = $order->getCompanySendInUsa();
        //$data->trackingNumberInUsa = $order->getSystemNumInUsa();
        $data->comment = $order->getComment();
        $data->address = $order->getAddress();

        if($order->getOrderType()->getCode() == 'express'){
            $data->sendDetailWeight=$this->getWeightInKg($order->getSendDetailWeight());
            $data->sendDetailLength=$order->getSendDetailLength();
            $data->sendDetailWidth=$order->getSendDetailWidth();
            $data->sendDetailHeight=$order->getSendDetailHeight();
        }else{
            {
                list($lbWeight,$ozWeight)=$this->getWeightInLb($order->getSendDetailWeight());
                $data->weightLb=$lbWeight??0;
                $data->weightOz=$ozWeight??0;
                $data->size1=$order->getSendDetailLength();
                $data->size2=$order->getSendDetailWidth();
                $data->size3=$order->getSendDetailHeight();
            }
        }

        $data->orderType=$order->getOrderType()->getOriginName();

        $data->productsData = [];
        foreach ($order->getProducts() as $product) {
            $data->productsData[]  = [
                'descrEn' => $product->getDescEn(),
                'descrUa' => $product->getDescEn(),
                'count' => $product->getCount(),
                'price' => $product->getPrice(),
            ];
        }

        $data_json = json_encode($data);
        $headers = array(
            'Authorization: ' . $this->api_key,
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_json)
        );
        $requestUrl = '';
        /*
        if($order->getOrderType()->getCode() == 'econom'){
            $requestUrl = $this->api_base_url.$this->path_econom;
        }
        */
        if($order->getOrderType()->getCode() == 'express'){
            $requestUrl = $this->api_base_url.$this->path_express;
        }else $requestUrl = $this->api_base_url.$this->path_econom;

        $curlObj = curl_init();
        curl_setopt($curlObj, CURLOPT_URL,$requestUrl);
        curl_setopt($curlObj, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curlObj, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curlObj, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlObj,CURLOPT_SSL_VERIFYPEER, false);

        $response  = curl_exec($curlObj);
        curl_close($curlObj);
        unset($curlObj);
        if(json_decode($response)->status != 'success') {
            error_log('------START-----' . date('Y-m-d H:i') . PHP_EOL, 3, LOG_SKLADUSA);
            error_log('------response-----' . $response . PHP_EOL, 3, LOG_SKLADUSA);
        }
        return $response;

    }

    public function calculate($data)
    {
        $data_json = json_encode($data);
        $headers = array(
            'Authorization: ' . $this->api_key,
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_json)
        );

        $curlObj = curl_init();
        curl_setopt($curlObj, CURLOPT_URL,$this->api_base_url.$this->path_calculate);
        curl_setopt($curlObj, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curlObj, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curlObj, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlObj,CURLOPT_SSL_VERIFYPEER, false);

        $response  = curl_exec($curlObj);
        curl_close($curlObj);
        unset($curlObj);
        if(json_decode($response)->status != 'success') {
            error_log('------START-----' . date('Y-m-d H:i') . PHP_EOL, 3, LOG_SKLADUSA);
            error_log('------response-----' . $response . PHP_EOL, 3, LOG_SKLADUSA);
        }
        return json_decode($response,1);
    }


    public function getWeightInKg($weigth)
    {
        return $weigth/1000;
    }

    public function getWeightInLb($weigth)
    {
        $ozFullWeight=floor($weigth * 0.035274);
        $lbWeight=floor($ozFullWeight /16);
        $ozWeight= $ozFullWeight-$lbWeight*16;

        return [$lbWeight,$ozWeight];
    }
}