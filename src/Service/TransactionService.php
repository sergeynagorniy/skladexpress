<?php

namespace App\Service;

use App\Entity\Invoices;
use App\Entity\Order;
use App\Entity\OrderStatus;
use App\Entity\Transaction;
use App\Entity\User;
use App\Helper\Arr;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;

use Twig\Environment;

define("TRANSACTION_LOG", getcwd() . "/../transaction.log");

class TransactionService
{
    /** @var EntityManagerInterface  */
    private $entityManager;

    private $twig;

    /** @var User */
    private $user;
    /** @var Invoices */
    private $invoice;

    /** @var array */
    private $data;

    static $sucessArray=[
        "success"
    ];

    private $balanceService;

    public function __construct(
        EntityManagerInterface $entityManager,
        Environment $twig,
        BalanceService $balanceService
        )
    {
        $this->entityManager = $entityManager;
        $this->twig = $twig;
        $this->balanceService = $balanceService;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @param Invoices $invoice
     */
    public function setInvoice(Invoices $invoice): void
    {
        $this->invoice = $invoice;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * $data =[
     *  "number",
     *  "summ",
     *  "firstName",
     *  "lastName",
     *  "phoneNumber",
     *  "status",
     *  "payInfo",
     *  "user",
     *  "invoice",
     *  "invoiceStr"
     * ],
     * @param array $data
     * @return Transaction
     */

    public function saveTransaction()
    {
        $data = $this->data;
        $trAutorize = $this->getEntityManager()->getRepository(Transaction::class)->findOneBy(['number'=>$data['number']]);

        if (empty($trAutorize)) {
            $trAutorize = new Transaction();
            $trAutorize
                ->setCreatedAt(new \DateTime())
                ->setNumber($data["number"])
                ->setSum($data["totalSumm"] - $data["comission"])
                ->setTotalSum($data["totalSumm"])
                ->setPerTransactionFee($data["pertrfee"])
                ->setComission($data["comission"])
                ->setFirstName($data["firstName"])
                ->setLastName($data["lastName"])
                ->setPhoneNumber($data["phoneNumber"])
                ->setPayInfo($data['payInfo'])
                ->setLiqpayOrderId('')
                ->setStatus($data['status'])
                ->setPaymentSystem((int)Arr::get($data,'paymentSystem',0))
            ;
        }
        if ($this->invoice && $this->invoice instanceof Invoices){
            $trAutorize->setInvoice($this->invoice);
        }

        if ($this->user && $this->user instanceof User){
            $trAutorize->setUser($this->user);
        }
        error_log(date('c')."--".$data["number"] . PHP_EOL, 3, TRANSACTION_LOG);
        $this->getEntityManager()->persist($trAutorize);
        $this->getEntityManager()->flush();

        if ($this->user && $this->user instanceof User && in_array($data['status'],self::$sucessArray) && !$this->invoice->isPaid()){
            $this->balanceService->addMoney($this->user,$trAutorize->getSum(),"New payment add id:".$trAutorize->getId(),null,$trAutorize);
            if ($this->invoice && $this->invoice instanceof Invoices){
                $this->invoice->setIsPaid(true);
                $this->getEntityManager()->persist( $this->invoice);
                $this->getEntityManager()->flush();
                if ($this->invoice->getOrderId()) {
                    $this->balanceService->minusMoney($this->user, $this->invoice->getPrice(), 'Pay invoice id: ' . $this->invoice->getId(), null, $this->invoice);
                    $this->payOrderStatus($this->invoice->getOrderId());
                }
                $this->getEntityManager()->flush();
            }
        }

    return $trAutorize;
    }


    /**
 * @param Order $order
 */
    public function payOrderStatus($order)
    {
        if ($order && $this->isOrderPaid($order)){
            /** @var  OrderStatus $orderStatus */
            $orderStatus = $this->getEntityManager()->getRepository(OrderStatus::class)->findOneBy(['status'=>'paid']);
            $order->setOrderStatus($orderStatus);
            if (empty($order->getTrNum())) $order->setTrNum($this->generateTrNumber($order->getId()));
            $this->getEntityManager()->persist($order);
        }
    }


    /**
     * @param Order $order
     * @return bool
     */
    public function isOrderPaid($order)
    {
        if (!$order->getInvoices()->isEmpty()){
            /** @var Invoices $invoice */
            foreach ($order->getInvoices() as $invoice){
                if (!$invoice->isPaid() && !$invoice->isRefund())  return false;
            }
        }
        return true;
    }
    /**
     * @param Order $order
     */
    public function newInvoiceOrderStatus($order)
    {
        if ($order){
            /** @var  OrderStatus $orderStatus */
            $orderStatus = $this->getEntityManager()->getRepository(OrderStatus::class)->findOneBy(['status'=>'getnewinvoice']);
            $order->setOrderStatus($orderStatus);
            $this->getEntityManager()->persist($order);
        }
    }

    /**
     * @param string $testStr
     * @return int
     *
     */
    public function getClearInvoiceId()
    {
        $testStr = $this->data["invoiceStr"]??'';
        $arrTmp = explode("_", $testStr);
        if ($arrTmp[0] == 'EXPRESSINVOICE' && count($arrTmp) >= 2) {
            $invoiceId = $arrTmp[1] ?? null;
            if ($invoiceId) {
                return (int)$invoiceId;
            }
            return $testStr;
        }
    }


        public function generateTrNumber($id)
        {
            return "EP".($id+57354658)."UA";
        }


}
