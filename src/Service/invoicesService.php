<?php

namespace App\Service;

use App\Entity\BalanceLog;
use App\Entity\FormHelps;
use App\Entity\Invoices;
use App\Entity\Order;
use App\Entity\OrderStatus;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;

use Twig\Environment;

class invoicesService
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var BalanceService */
    private $balanceService;

    /** @var TransactionService */
    private $transactionService;

    public function __construct(
        EntityManagerInterface $entityManager,
        BalanceService $balanceService,
        TransactionService $transactionService
    )
    {
        $this->entityManager = $entityManager;
        $this->balanceService = $balanceService;
        $this->transactionService = $transactionService;
    }

    /**
     * @param Order $order
     * @param bool $isShipping
     */
    public function createInvoice($order, $isShipping = false)
    {
        /** @var Invoices $invoice */
        $invoice = $this->entityManager->getRepository(Invoices::class)->findOneBy(["shippingPay" => $isShipping, "refund" => false, "orderId" => $order]);

        if ($isShipping) {
            $price = $order->getShippingCosts();
        } else {
            $price = $order->getDeclareValue();
        }

        if ($invoice && !$invoice->isPaid() && $invoice->getPrice() <> $price) {
            $invoice->setPrice($price);
        } elseif ($invoice && $invoice->isPaid() && $invoice->getPrice() <> $price) {
            $this->refundInvoice($invoice);
            $this->payInvoice($invoice);
            $invoice = null;
        }

        if (empty($invoice)) {
            $invoice = new Invoices();
            $invoice
                ->setUser($order->getUser())
                ->setPrice($price)
                ->setShippingPay($isShipping)
                ->setOrderId($order)
                ->setNoComission(true);
        }

        $this->entityManager->persist($invoice);
        $this->entityManager->flush();
        try {
            $order->addInvoice($invoice);
            $this->entityManager->persist($order);
            $this->entityManager->flush();

            if ($invoice->getPrice() > 0 && $invoice->getPrice() <= $this->getUserFromInvoice($invoice)->getBalance()) {
                $this->payInvoice($invoice);
            } else {
                $this->transactionService->newInvoiceOrderStatus($invoice->getOrderId());
            }
        } catch (\Exception $exception) {

        }
    }

    /**
     * @param Invoices $invoice
     */
    public function refundInvoice($invoice)
    {
        $invoice->setRefund(true);
        $this->entityManager->persist($invoice);
        $this->entityManager->flush();
    }

    /**
     * @param Invoices $invoice
     */
    public function payInvoice($invoice)
    {
        try {
            if ($invoice->isRefund()) {
                $this->balanceService->addMoney($this->getUserFromInvoice($invoice), $invoice->getPrice(), 'Refund invoice id: ' . $invoice->getId(), null, $invoice);

                $invoice->setIsPaid(true);
                $this->entityManager->persist($invoice);
                $this->transactionService->payOrderStatus($invoice->getOrderId());
                $this->entityManager->flush();
            }

        } catch (\Exception $exception) {

        }
    }

    /**
     * @param Invoices $invoice
     * @return User
     */
    public function getUserFromInvoice($invoice)
    {
        $user = $invoice->getUser();
        if (empty($user)) $user = ($invoice->getOrderId()) ? $invoice->getOrderId()->getUser() : null;

        return $user;

    }

    public function payInvoiceFromBalance($invoice)
    {
        try {
            $this->balanceService->minusMoney($this->getUserFromInvoice($invoice), $invoice->getPrice(), 'Pay invoice id: ' . $invoice->getId(), null, $invoice);
            $invoice->setIsPaid(true);
            $this->entityManager->persist($invoice);
            $this->entityManager->flush();
            $this->transactionService->payOrderStatus($invoice->getOrderId());
            $this->entityManager->flush();
        } catch (\Exception $exception) {

        }
    }
}
