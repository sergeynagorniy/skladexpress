<?php

namespace App\Traits;



trait PriceTrait
{

    public function getPriceWithCommissionPayPal($price)
    {
        $price = $price ?? 0;
        $price +=0.3;

        return $this->formatAmount($price + ($price * 3) / 100);

    }

    public function getPriceWithCommissionAuthorizeNet($price)
    {
        $price = $price ?? 0;
        $price += 0.3;
        return $this->formatAmount(($price + ($price * 4.16) / 100));

    }

    public function formatAmount($price)
    {
        return ceil($price*100)/100;
    }

    public function getCommissionAuthorizeNet($price)
    {
        $price = $price ?? 0;

        return $this->formatAmount($price * 4.16 / 100 + 0.3 );
    }
}
